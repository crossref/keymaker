package org.crossref.keymaker

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@Disabled
class KeymakerApplicationTests {
    @Test
    fun contextLoads() {
    }
}
