package org.crossref.keymaker.api

import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import java.util.UUID
import javax.ws.rs.NotAuthorizedException

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
    properties = [
        "server.port=9191", "management.server.port=9042", "keycloak.admin.realm=testrealm"
    ]
)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class UserAdminIntegrationTests : IntegrationTester() {
    companion object {
        val users: MutableMap<String, User> = mutableMapOf()
        val randomUser = "user${kotlin.random.Random.nextInt(0,10000)}@crossref.com"
    }

    private inline fun <reified T : Any> doReq(method: String, path: String, token: String, data: Any? = null): Pair<Int, T> =
        with(prepareReq(method, path, token, data).response().second) {
            Pair<Int, T> (this.statusCode, mapper.readValue<T>(this.data))
        }

    @Test
    @Order(10)
    fun `find all users in the system`() {
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user",
            staffToken
        ).second.also {

            assertTrue(it.size > 0, "No users have been found")
            assertTrue(it.any { it.username == "user@crossref.org" }, "User user@crossref.org not found")
        }
    }

    @Test
    @Order(20)
    fun `find users in the system using filters`() {
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user?username=user@cr",
            staffToken
        ).second.first().also {
            assertEquals(
                "user@crossref.org",
                it.username,
                "Username filtering failed for user user@crossref.org"
            )
        }
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user?first-name=john",
            staffToken
        ).second.first().also {
            assertEquals(
                "user@crossref.org",
                it.username,
                "Username filtering failed for user user@crossref.org"
            )
        }
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user?last-name=smith",
            staffToken
        ).second.first().also {
            assertEquals(
                "user@crossref.org",
                it.username,
                "Username filtering failed for user user@crossref.org"
            )
        }
    }

    @Test
    @Order(30)
    fun `Add user works`() {
        users["user1"] = doReq<User>(
            "POST",
            "/keymaker/api/v1/user",
            staffToken,
            User(UUID.randomUUID(), "random@user.com", "random", "user")
        ).second

        assertEquals("random@user.com", users["user1"]!!.username)
    }

    @Test
    @Order(30)
    fun `Add user with invalid username fails`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/user",
            staffToken,
            User(UUID.randomUUID(), "random@user", "random", "user")
        ).also {
            checkStatus(it, HttpStatus.BAD_REQUEST)
        }
    }

    @Test
    @Order(30)
    fun `Update user`() {
        val updatedUser = User(
            users["user1"]!!.userId,
            users["user1"]!!.username + ".org",
            users["user1"]!!.firstName + "_updated",
            users["user1"]!!.lastName + "_updated"
        )
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/user/${users["user1"]!!.userId}",
                staffToken,
                updatedUser
            ).statusCode
        )
        doReq<User>("GET", "/keymaker/api/v1/user/${users["user1"]!!.userId}", staffToken).second.also {
            assertEquals(users["user1"]!!.username, it.username, "Username should not be able to change")
            assertEquals(updatedUser.firstName, it.firstName, "firstName did not change")
            assertEquals(updatedUser.lastName, it.lastName, "lastName did not change")
        }
    }

    @Test
    @Order(35)
    fun `Update non existent user fails`() {
        val updatedUser = User(
            users["user1"]!!.userId,
            users["user1"]!!.username + ".org",
            users["user1"]!!.firstName + "_updated",
            users["user1"]!!.lastName + "_updated"
        )
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/user/${UUID.randomUUID()}",
            staffToken,
            updatedUser
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                ".*User.*"
            )
        }
    }

    @Test
    @Order(198)
    fun `Non PWDSYNC roles cannot sync password`() {
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "POST",
                "/keymaker/api/v1/user/syncpwd",
                userToken,
                HashSyncMessage(
                    randomUser,
                    "OTk5ODlmZDFhOGE3",
                    "Q16jZscBoLY+02XUfjRTZCb/ZiAT7OOFMxrSbmDzxIYZsaVyLLd73rfncc00bVVCa54I/x+pmUEUbSjXNoS2Ww==",
                    "pbkdf2-sha512",
                    150000
                )
            ).statusCode
        )

        assertThrows(NotAuthorizedException::class.java) {
            getKeycloakToken(randomUser, "crossref123.")
        }

        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "POST",
                "/keymaker/api/v1/user/syncpwd",
                staffToken,
                HashSyncMessage(
                    randomUser,
                    "OTk5ODlmZDFhOGE3",
                    "Q16jZscBoLY+02XUfjRTZCb/ZiAT7OOFMxrSbmDzxIYZsaVyLLd73rfncc00bVVCa54I/x+pmUEUbSjXNoS2Ww==",
                    "pbkdf2-sha512",
                    150000
                )
            ).statusCode
        )
        assertThrows(NotAuthorizedException::class.java) {
            getKeycloakToken(randomUser, "crossref123.")
        }
    }

    @Test
    @Order(199)
    fun `User pwd sync woks for user`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "POST",
                "/keymaker/api/v1/user/syncpwd",
                syncToken,
                HashSyncMessage(
                    "user2@crossref.org",
                    "OTk5ODlmZDFhOGE3",
                    "Q16jZscBoLY+02XUfjRTZCb/ZiAT7OOFMxrSbmDzxIYZsaVyLLd73rfncc00bVVCa54I/x+pmUEUbSjXNoS2Ww==",
                    "pbkdf2-sha512",
                    150000
                )
            ).statusCode
        )

        getKeycloakToken("user2@crossref.org", "crossref123.")
    }

    @Test
    @Order(200)
    fun `Delete user`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/user/${users["user1"]!!.userId}",
                staffToken
            ).statusCode
        )
        doReq<ResponseMessage>(
            "GET",
            "/keymaker/api/v1/user/${users["user1"]!!.userId}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                ".*User.*"
            )
        }
    }

    @Test
    @Order(210)
    fun `Delete non existent user fails`() {
        doReq<ResponseMessage>(
            "DELETE",
            "/keymaker/api/v1/user/${UUID.randomUUID()}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                ".*User.*"
            )
        }
    }

    @Test
    @Order(300)
    fun `User created in keymaker creates a user in authenticator if it does not exist`() {
        assertFalse(authenticatorUserExists(randomUser))

        doReq<User>(
            "POST",
            "/keymaker/api/v1/user",
            IntegrationTester.staffToken,
            User(UUID.randomUUID(), randomUser, "random", "user")
        ).second

        assertTrue(authenticatorUserExists(randomUser))
    }
}
