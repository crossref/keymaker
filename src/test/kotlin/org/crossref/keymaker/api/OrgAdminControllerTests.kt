package org.crossref.keymaker.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.Called
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.just
import io.mockk.verify
import org.crossref.keymaker.service.KeyAdminService
import org.crossref.keymaker.service.OrgAdminService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(OrgAdminController::class)
class OrgAdminControllerTests(@Autowired val mockMvc: MockMvc, @Autowired val mapper: ObjectMapper) {
    @MockkBean
    lateinit var orgAdminService: OrgAdminService
    @MockkBean
    lateinit var keyAdminService: KeyAdminService

    @BeforeEach
    fun `reset mocks`() {
        clearAllMocks()
    }

    @Test
    fun `unfiltered org search with valid staff token returns all orgs`() {
        every { orgAdminService.findOrgs(emptyMap()) } returns orgs

        mockMvc.perform(
            get("/keymaker/api/v1/organization").with(mockToken("ROLE_STAFF", "ROLE_USER"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(orgs)))
    }

    @Test
    fun `unfiltered org search with valid user token returns one org`() {
        every { orgAdminService.findOrgs(emptyMap(), any()) } returns listOf(orgs[0])

        mockMvc.perform(
            get("/keymaker/api/v1/organization").with(mockToken("ROLE_USER"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(listOf(orgs[0]))))
    }

    @Test
    fun `filtered org search with valid token returns filtered orgs`() {
        val filteredOrgs = listOf(orgs[0])
        every {
            orgAdminService.findOrgs(
                mapOf(
                    "orgId" to orgs[0].orgId.toString(),
                    "name" to "Org 1"
                )
            )
        } returns filteredOrgs

        mockMvc.perform(
            get("/keymaker/api/v1/organization").with(mockToken("ROLE_STAFF", "ROLE_USER")).param("orgId", orgs[0].orgId.toString())
                .param("name", "Org 1")
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(filteredOrgs)))
    }

    @Test
    fun `controller handles AuthServiceNotAvailableException`() {
        val ex = AuthServiceUnavailableException("HttpHostConnectException: localhost:8888: connection refused")
        every { orgAdminService.findOrgs(any()) } throws ex

        mockMvc.perform(
            get("/keymaker/api/v1/organization").with(mockToken("ROLE_STAFF", "ROLE_USER"))
        ).andExpect(status().isServiceUnavailable).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.SERVICE_UNAVAILABLE, "Authorization service unavailable", listOf(ex.message)
                        )
                    )
                )
            )
    }

    @Test
    fun `org search with missing token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization")
        ).andExpect(status().isUnauthorized)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `org search with unauthorized token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org with valid token returns single org`() {
        every { orgAdminService.getOrg(orgs[0].orgId!!) } returns orgs[0]

        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(orgs[0])))
    }

    @Test
    fun `controller handles OrgNotFoundException`() {
        every { orgAdminService.getOrg(orgs[0].orgId!!) } throws OrgNotFoundException(orgs[0].orgId!!)

        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isNotFound).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.NOT_FOUND, "Organization with ID ${orgs[0].orgId} not found", emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `get org with missing token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}")
        ).andExpect(status().isUnauthorized)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org with unauthorized token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `add org with valid token completes successfully`() {
        every { orgAdminService.addOrg(orgs[0]) } returns orgs[0]
        mockMvc.perform(
            post("/keymaker/api/v1/organization").with(mockToken("ROLE_STAFF")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(orgs[0]))
        ).andExpect(status().isCreated)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(orgs[0])))
    }

    @Test
    fun `controller handles OrgAlreadyExistsException`() {
        every { orgAdminService.addOrg(orgs[0]) } throws OrgAlreadyExistsException(
            orgs[0].orgId!!,
            orgs[0].name,
            orgs[0].sugarId
        )

        mockMvc.perform(
            post("/keymaker/api/v1/organization").with(mockToken("ROLE_STAFF")).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(orgs[0]))
        ).andExpect(status().isConflict).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.CONFLICT,
                            "Organization with name ${orgs[0].name} and / or Sugar ID ${orgs[0].sugarId} already exists with ID ${orgs[0].orgId}",
                            emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `add org with missing token fails as expected`() {
        mockMvc.perform(
            post("/keymaker/api/v1/organization")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `add org with unauthorized token fails as expected`() {
        mockMvc.perform(
            post("/keymaker/api/v1/organization").with(mockToken()).contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(orgs[0]))
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `update org with valid token completes successfully`() {
        every { orgAdminService.updateOrg(orgs[0]) } just Runs
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken("ROLE_STAFF"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(orgs[0]))
        ).andExpect(status().isNoContent)

        verify { orgAdminService.updateOrg(orgs[0]) }
    }

    @Test
    fun `update org with missing token fails as expected`() {
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `update org with unauthorized token fails as expected`() {
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(orgs[0]))
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `delete org with valid token completes successfully`() {
        every { orgAdminService.deleteOrg(orgs[0].orgId!!) } just Runs
        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isNoContent)

        verify { orgAdminService.deleteOrg(orgs[0].orgId!!) }
    }

    @Test
    fun `delete org with missing token fails as expected`() {
        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `delete org with unauthorized token fails as expected`() {
        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org users with valid token returns org users`() {
        every { orgAdminService.getOrgUsers(orgs[0].orgId!!) } returns users

        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/users").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(users)))
    }

    @Test
    fun `get org users with missing token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/users")
        ).andExpect(status().isUnauthorized)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org users with unauthorized token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/users").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org admins with valid token returns org admins`() {
        every { orgAdminService.getOrgAdmins(orgs[0].orgId!!) } returns users

        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/admins").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(users)))
    }

    @Test
    fun `get org admins with missing token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/admins")
        ).andExpect(status().isUnauthorized)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org admins with unauthorized token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/admins").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `add org admin user with valid token completes successfully`() {
        every {
            orgAdminService.addOrgAdminUser(
                orgs[0].orgId!!,
                users[0].userId!!
            )
        } just Runs

        mockMvc.perform(
            post("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isNoContent)

        verify {
            orgAdminService.addOrgAdminUser(
                orgs[0].orgId!!,
                users[0].userId!!
            )
        }
    }

    @Test
    fun `controller handles UserNotFoundException`() {
        every {
            orgAdminService.addOrgAdminUser(
                orgs[0].orgId!!,
                users[0].userId!!
            )
        } throws UserNotFoundException(users[0].userId!!)

        mockMvc.perform(
            post("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isNotFound).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.NOT_FOUND, "User with ID ${users[0].userId} not found", emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `controller handles UserAlreadyOrgAdminException`() {
        every {
            orgAdminService.addOrgAdminUser(
                orgs[0].orgId!!,
                users[0].userId!!
            )
        } throws UserAlreadyOrgAdminException(orgs[0].orgId!!, users[0].userId!!)

        mockMvc.perform(
            post("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isConflict).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.CONFLICT,
                            "User with ID ${users[0].userId} is already an administrator of organization with ID ${orgs[0].orgId}",
                            emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `add org admin user with missing token fails as expected`() {
        mockMvc.perform(
            post("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `add org admin user with unauthorized token fails as expected`() {
        mockMvc.perform(
            post("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `remove org admin user with valid token completes successfully`() {
        every { orgAdminService.removeOrgAdminUser(orgs[0].orgId!!, users[0].userId!!) } just Runs

        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isNoContent)

        verify {
            orgAdminService.removeOrgAdminUser(
                orgs[0].orgId!!,
                users[0].userId!!
            )
        }
    }

    @Test
    fun `controller handles UserNotOrgAdminException`() {
        every {
            orgAdminService.removeOrgAdminUser(
                orgs[0].orgId!!,
                users[0].userId!!
            )
        } throws UserNotOrgAdminException(orgs[0].orgId!!, users[0].userId!!)

        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken("ROLE_STAFF"))
        ).andExpect(status().isConflict).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(
                content().json(
                    mapper.writeValueAsString(
                        ResponseMessage(
                            HttpStatus.CONFLICT,
                            "User with ID ${users[0].userId} is not an administrator of organization with ID ${orgs[0].orgId}",
                            emptyList()
                        )
                    )
                )
            )
    }

    @Test
    fun `remove org admin user with missing token fails as expected`() {
        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `remove org admin user with unauthorized token fails as expected`() {
        mockMvc.perform(
            delete("/keymaker/api/v1/organization/${orgs[0].orgId}/admins/${users[0].userId}").with(mockToken())
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `enable org subscription with valid token completes successfully`() {
        every { orgAdminService.setOrgSubscriptionStatus(orgs[0].orgId!!, Subscription.PLUS, true) } just Runs

        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/enable").with(
                mockToken("ROLE_STAFF")
            )
        ).andExpect(status().isNoContent)

        verify { orgAdminService.setOrgSubscriptionStatus(orgs[0].orgId!!, Subscription.PLUS, true) }
    }

    @Test
    fun `controller handles enable OrgSubscriptionUnchangedException`() {
        every {
            orgAdminService.setOrgSubscriptionStatus(
                orgs[0].orgId!!,
                Subscription.PLUS,
                true
            )
        } throws OrgSubscriptionUnchangedException(orgs[0].orgId!!, Subscription.PLUS, true)

        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/enable").with(
                mockToken("ROLE_STAFF")
            )
        ).andExpect(status().isConflict).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.CONFLICT,
                        "Organization with ID ${orgs[0].orgId} is already subscribed to ${Subscription.PLUS}",
                        emptyList()
                    )
                )
            )
        )
    }

    @Test
    fun `enable org subscription with missing token fails as expected`() {
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/enable")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `enable org subscription with unauthorized token fails as expected`() {
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/enable").with(
                mockToken()
            )
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `disable org subscription with valid token completes successfully`() {
        every { orgAdminService.setOrgSubscriptionStatus(orgs[0].orgId!!, Subscription.PLUS, false) } just Runs

        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/disable").with(
                mockToken("ROLE_STAFF")
            )
        ).andExpect(status().isNoContent)

        verify { orgAdminService.setOrgSubscriptionStatus(orgs[0].orgId!!, Subscription.PLUS, false) }
    }

    @Test
    fun `controller handles disable OrgSubscriptionUnchangedException`() {
        every {
            orgAdminService.setOrgSubscriptionStatus(
                orgs[0].orgId!!,
                Subscription.PLUS,
                false
            )
        } throws OrgSubscriptionUnchangedException(orgs[0].orgId!!, Subscription.PLUS, false)

        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/disable").with(
                mockToken("ROLE_STAFF")
            )
        ).andExpect(status().isConflict).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.CONFLICT,
                        "Organization with ID ${orgs[0].orgId} is already unsubscribed from ${Subscription.PLUS}",
                        emptyList()
                    )
                )
            )
        )
    }

    @Test
    fun `disable org subscription with missing token fails as expected`() {
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/disable")
        ).andExpect(status().isForbidden)

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `disable org subscription with unauthorized token fails as expected`() {
        mockMvc.perform(
            put("/keymaker/api/v1/organization/${orgs[0].orgId}/subscription/${Subscription.PLUS}/disable").with(
                mockToken()
            )
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org keys with unauthorized token fails`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/keys").with(
                mockToken()
            )
        ).andExpect(status().isUnauthorized).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(
            content().json(
                mapper.writeValueAsString(
                    ResponseMessage(
                        HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource", emptyList()
                    )
                )
            )
        )

        verify { orgAdminService wasNot Called }
    }

    @Test
    fun `get org keys with missing token fails as expected`() {
        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/keys")
        ).andExpect(status().isUnauthorized)

        verify { orgAdminService wasNot Called }
        verify { keyAdminService wasNot Called }
    }

    @Test
    fun `get org keys with valid staff token returns all org keys`() {
        every { keyAdminService.findKeys(any()) } returns keys

        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/keys").with(mockToken("ROLE_STAFF", "ROLE_USER"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(keys)))
    }

    @Test
    fun `get org keys with valid user token returns all org keys`() {
        every { keyAdminService.findKeys(any(), any()) } returns keys

        mockMvc.perform(
            get("/keymaker/api/v1/organization/${orgs[0].orgId}/keys").with(mockToken("ROLE_USER"))
        ).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().json(mapper.writeValueAsString(keys)))
    }
}
