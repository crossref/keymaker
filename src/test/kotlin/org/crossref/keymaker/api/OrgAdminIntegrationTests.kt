package org.crossref.keymaker.api

import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.google.common.hash.Hashing
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.util.UUID

@SpringBootTest(
    webEnvironment = WebEnvironment.DEFINED_PORT,
    properties = [
        "server.port=9191", "management.server.port=9042", "keycloak.admin.realm=testrealm"
    ]
)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class OrgAdminIntegrationTests : IntegrationTester() {
    companion object {
        val initialOrgs: MutableMap<String, Organization> = mutableMapOf()
        val users: MutableMap<String, User> = mutableMapOf()
        val keys: MutableMap<String, Key> = mutableMapOf()
        val tokens: MutableMap<String, String> = mutableMapOf()
    }

    private inline fun <reified T : Any> doReq(method: String, path: String, token: String, data: Any? = null): Pair<Int, T> =
        with(prepareReq(method, path, token, data).response().second) {
            Pair<Int, T> (this.statusCode, mapper.readValue<T>(this.data))
        }

    @Test
    @Order(10)
    fun `get organizations without credentials`() {
        val resp = "http://localhost:9191/keymaker/api/v1/organization/".httpGet().response().second
        assertEquals(HttpStatus.UNAUTHORIZED.value(), resp.statusCode, "Wrong HTTP status code")
    }

    @Test
    @Order(20)
    fun `creating organizations succeeds`() {
        val org1 = "org_${UUID.randomUUID()}"
        val org2 = "org_${UUID.randomUUID()}"
        val org3 = "org_${UUID.randomUUID()}"
        initialOrgs["org1"] = doReq<Organization>(
            "POST", "/keymaker/api/v1/organization/", staffToken,
            Organization(
                UUID.randomUUID(), org1, 1,
                "org1"
            )
        ).second
        initialOrgs["org2"] = doReq<Organization>(
            "POST", "/keymaker/api/v1/organization/", staffToken,
            Organization(
                UUID.randomUUID(), org2, 2,
                "org2"
            )
        ).second
        initialOrgs["org3"] = doReq<Organization>(
            "POST", "/keymaker/api/v1/organization/", staffToken,
            Organization(
                UUID.randomUUID(), org3, 3,
                "org3"
            )
        ).second

        assertEquals(org1, initialOrgs["org1"]!!.name, "Name for org1 does not match")
        assertEquals(org2, initialOrgs["org2"]!!.name, "Name for org2 does not match")
        assertEquals(org3, initialOrgs["org3"]!!.name, "Name for org2 does not match")
    }

    @Test
    @Order(30)
    fun `get organizations works`() {
        assertTrue(
            doReq<List<Organization>>("GET", "/keymaker/api/v1/organization/", staffToken).second.any { it.name.equals(initialOrgs["org1"]!!.name) },
            "Organization Org1 not found"
        )
    }

    @Test
    @Order(40)
    fun `get non-existent org fails`() {
        doReq<ResponseMessage>("GET", "/keymaker/api/v1/organization/${UUID.randomUUID()}", staffToken).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(50)
    fun `creating existing organization fails`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/organization/",
            staffToken,
            Organization(UUID.randomUUID(), initialOrgs["org1"]!!.name, 1, "org1")
        ).also {
            checkStatus(
                it,

                HttpStatus.CONFLICT,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(60)
    fun `Updating an organization works`() {
        val updatedOrg = with(initialOrgs["org1"]!!) {
            Organization(
                this.orgId,
                this.name + "_updated",
                this.sugarId + 1,
                "",
                mutableListOf(Subscription.PLUS)
            )
        }

        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${updatedOrg.orgId}",
                staffToken,
                updatedOrg
            ).statusCode
        )

        doReq<Organization>("GET", "/keymaker/api/v1/organization/${updatedOrg.orgId}", staffToken).also {
            assertEquals(updatedOrg.name, it.second.name, "Name has not been updated")
            assertEquals(updatedOrg.description, it.second.description, "Description has not been updated")
            assertEquals(listOf<Subscription>(), it.second.subscriptions, "Subscriptions shouldn't be updated")
        }
    }

    @Test
    @Order(70)
    fun `Updating a non-existent organization fails`() {
        val updatedOrg = with(initialOrgs["org1"]!!) {
            Organization(
                this.orgId,
                this.name + "_updated",
                this.sugarId + 1,
                "",
                mutableListOf(Subscription.PLUS)
            )
        }

        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}",
            staffToken,
            updatedOrg
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(80)
    fun `Adding org users works`() {
        val (_, lo) = doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/users/",
            staffToken
        )

        assertTrue(lo.isEmpty(), "User list is not empty")

        users["user1"] = doReq<User>(
            "POST",
            "/keymaker/api/v1/user",
            staffToken,
            User(UUID.randomUUID(), "org1user@org1.com", "fname", "lname")
        ).second

        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "POST",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${users["user1"]!!.userId}",
                staffToken
            ).statusCode
        )

        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/users/",
            staffToken
        ).second.also {
            assertEquals(1, it.size, "org1 should have exactly one user")
            assertEquals(users["user1"]!!.username, it.first().username, "Usernames do not match")
        }
    }

    @Test
    @Order(90)
    fun `Adding user twice as org admin fails`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${users["user1"]!!.userId}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.CONFLICT,
                ".*already an administrator.*"
            )
        }
    }

    @Test
    @Order(100)
    fun `Adding org users wrong org fails`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/admins/${UUID.randomUUID()}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(110)
    fun `Adding org users wrong user fails`() {
        doReq<ResponseMessage>(
            "POST",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${UUID.randomUUID()}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "User.*"
            )
        }
    }

    @Test
    @Order(120)
    fun `List user and admins in an org works`() {
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/",
            staffToken
        ).second.also {
            assertEquals(1, it.size, "org1 should have exactly one user")
            assertEquals(users["user1"]!!.username, it.first().username, "Usernames do not match")
        }
    }

    @Test
    @Order(130)
    fun `List user or admins in an non existent org returns 404`() {
        doReq<ResponseMessage>(
            "GET",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/admins/",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
        doReq<ResponseMessage>(
            "GET",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/users/",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(140)
    fun `Delete admin from org works`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${users["user1"]!!.userId}",
                staffToken
            ).statusCode
        )
        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/",
            staffToken
        ).second.also {
            assertEquals(0, it.size, "org1 should not have admins")
        }
    }

    @Test
    @Order(150)
    fun `Delete admin from non existent org fails`() {
        doReq<ResponseMessage>(
            "DELETE",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/admins/${UUID.randomUUID()}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(160)
    fun `Delete non existent admin from  org fails`() {
        doReq<ResponseMessage>(
            "DELETE",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${UUID.randomUUID()}",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "User.*"
            )
        }
    }

    @Test
    @Order(170)
    fun `Enable subscription on an org works`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/enable",
                staffToken
            ).statusCode
        )

        doReq<Organization>(
            "GET",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}",
            staffToken
        ).second.also {
            assertEquals(
                listOf(Subscription.PLUS),
                it.subscriptions
            )
        }
    }

    @Test
    @Order(175)
    fun `Serch organization using filters`() {
        doReq<List<Organization>>("GET", "/keymaker/api/v1/organization/?name=g1", staffToken).second.also {
            assertEquals(1, it.size)
            assertEquals("org1", it.first().name)
        }

        doReq<List<Organization>>("GET", "/keymaker/api/v1/organization/?subscription=PLUS", staffToken).second.also {
            assertEquals(1, it.size)
            assertEquals(initialOrgs["org1"]!!.name + "_updated", it.first().name)
        }
    }

    @Test
    @Order(180)
    fun `Disable subscription on an org works`() {
        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/disable",
                staffToken
            ).statusCode
        )

        doReq<Organization>(
            "GET",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}",
            staffToken
        ).second.also {
            assertEquals(
                listOf<Subscription>(),
                it.subscriptions
            )
        }
    }

    @Test
    @Order(190)
    fun `Disable on a non existent org fails`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/subscription/PLUS/disable",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(200)
    fun `Enable on a non existent org fails`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/organization/${UUID.randomUUID()}/subscription/PLUS/enable",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.NOT_FOUND,
                "Organization.*"
            )
        }
    }

    @Test
    @Order(210)
    fun `double unsubscribe throws exception`() {
        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/disable",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.CONFLICT,
                ".*already unsubscribed.*"
            )
        }
    }

    @Test
    @Order(220)
    fun `double subscribe throws exception`() {
        execReq(
            "PUT",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/enable",
            staffToken
        )

        doReq<ResponseMessage>(
            "PUT",
            "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/enable",
            staffToken
        ).also {
            checkStatus(
                it,
                HttpStatus.CONFLICT,
                ".*already subscribed.*"
            )
        }
    }

    @Test
    @Order(230)
    fun `Test User can see only his orgs`() {
        doReq<List<Organization>>(
            "GET",
            "/keymaker/api/v1/organization",
            userToken,
        ).second.also {
            assertEquals(1, it.size)
            assertEquals("org1", it.first().name)
        }
    }

    @Test
    @Order(240)
    fun `Test User cannot call any operation apart from listing orgs`() {
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq("GET", "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}", userToken).statusCode,
            "User should be unathorized to fetch an organization object"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "POST",
                "/keymaker/api/v1/organization/",
                userToken,
                Organization(UUID.randomUUID(), "randomname", 999, "interesting"),
            ).statusCode,
            "User should be unauthorized to create an organization"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}",
                userToken,
                Organization(UUID.randomUUID(), "randomname", 999, "interesting")
            ).statusCode,
            "User should be unauthorized to update organizations"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "GET",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/users",
                userToken
            ).statusCode,
            "User should be unauthorized to retrieve organization users"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "GET",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins",
                userToken
            ).statusCode,
            "User should be unauthorized to retrieve organization admins"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}",
                userToken
            ).statusCode,
            "User should be unauthorized to delete organizations"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "POST",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${users["user1"]!!.userId}",
                userToken
            ).statusCode,
            "User should be unauthorized to add admin users"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/admins/${users["user1"]!!.userId}",
                userToken
            ).statusCode,
            "User should be unauthorized to delete admin users"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/enable",
                userToken
            ).statusCode,
            "User should be unauthorized to enable subscriptions"
        )
        assertEquals(
            HttpStatus.UNAUTHORIZED.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${initialOrgs["org1"]!!.orgId}/subscription/PLUS/disable",
                userToken
            ).statusCode,
            "User should be unauthorized to disable subscriptions"
        )
    }

    @Test
    @Order(300)
    fun `test permissions`() {
        val permissionsUrl = "$serverUrl/realms/testrealm/checkApiKeyPermissions"

        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user?username=user@crossref.org",
            staffToken
        ).also {
            users["user2"] = it.second.first()
        }

        execReq(
            "POST",
            "/keymaker/api/v1/organization/${initialOrgs["org2"]!!.orgId}/admins/${users["user2"]!!.userId}",
            staffToken
        )

        doReq<KeyToken>(
            "POST",
            "/keymaker/api/v1/key/${initialOrgs["org2"]!!.orgId}",
            userToken,
            Key(UUID.randomUUID(), "description1", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            keys["key1"] = it.second.key
            tokens["token1"] = it.second.token
            assertEquals("description1", it.second.key.description)
        }

        doReq<List<User>>(
            "GET",
            "/keymaker/api/v1/user?username=user@crossref.org",
            staffToken
        ).also {
            users["user2"] = it.second.first()
        }

        execReq(
            "POST",
            "/keymaker/api/v1/organization/${initialOrgs["org2"]!!.orgId}/admins/${users["user2"]!!.userId}",
            staffToken
        )

        doReq<KeyToken>(
            "POST",
            "/keymaker/api/v1/key/${initialOrgs["org2"]!!.orgId}",
            userToken,
            Key(UUID.randomUUID(), "description1", UUID.randomUUID(), LocalDateTime.now(), false)
        ).also {
            keys["key1"] = it.second.key
            tokens["token1"] = Hashing.sha256()
                .hashString((System.getenv("APIKEY_SALT") ?: "") + it.second.token, Charsets.US_ASCII)
                .toString()
                .lowercase()
            assertEquals("description1", it.second.key.description)
        }

        assertEquals(
            401,
            permissionsUrl.httpPost().jsonBody(
                mapper.writeValueAsString(
                    mapOf(
                        "apiKeyHash" to tokens["token1"],
                        "clientSecret" to "supersecret",
                        "clientName" to "content-system",
                        "resource" to "plus-data"
                    )
                )
            ).response().second.statusCode,
        )

        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "PUT",
                "/keymaker/api/v1/organization/${initialOrgs["org2"]!!.orgId}/subscription/PLUS/enable",
                staffToken
            ).statusCode
        )

        // Getting cached value
        assertEquals(
            401,
            permissionsUrl.httpPost().jsonBody(
                mapper.writeValueAsString(
                    mapOf(
                        "apiKeyHash" to tokens["token1"],
                        "clientSecret" to "supersecret",
                        "clientName" to "content-system",
                        "resource" to "plus-data"
                    )
                )
            ).response().second.statusCode,
        )

        // Allow keycloak cache to expire (10s)
        Thread.sleep(15000)

        assertEquals(
            200,
            permissionsUrl.httpPost().jsonBody(
                mapper.writeValueAsString(
                    mapOf(
                        "apiKeyHash" to tokens["token1"],
                        "clientSecret" to "supersecret",
                        "clientName" to "content-system",
                        "resource" to "plus-data"
                    )
                )
            ).response().second.statusCode,
        )

        assertEquals(
            HttpStatus.NO_CONTENT.value(),
            execReq(
                "DELETE",
                "/keymaker/api/v1/key/${keys["key1"]!!.keyId}",
                userToken,
            ).statusCode
        )
    }

    @Test
    @Order(990)
    fun `delete organizations`() {
        initialOrgs.values.forEach {
            assertEquals(
                204,
                execReq("DELETE", "/keymaker/api/v1/organization/${it.orgId}", staffToken).statusCode
            )
        }

        assertEquals(
            0,
            doReq<List<Organization>>("GET", "/keymaker/api/v1/organization/", staffToken).second
                .map { it.orgId }
                .toSet()
                .intersect(initialOrgs.map { it.value.orgId }.toSet()).size
        )
    }

    @Test
    @Order(1000)
    fun `delete non existent organizations return 404`() {
        initialOrgs.values.forEach {
            assertEquals(
                404,
                execReq("DELETE", "/keymaker/api/v1/organization/${it.orgId}", staffToken).statusCode
            )
        }

        execReq(
            "DELETE",
            "/keymaker/api/v1/user/${users["user1"]!!.userId}",
            staffToken
        )
    }
}
