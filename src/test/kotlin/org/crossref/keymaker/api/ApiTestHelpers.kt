package org.crossref.keymaker.api

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpDelete
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import org.crossref.keymaker.config.RealmSetup
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.common.util.MultivaluedHashMap
import org.keycloak.representations.idm.ComponentRepresentation
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.springframework.http.HttpStatus
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.JwtRequestPostProcessor
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt
import java.time.LocalDateTime
import java.util.UUID

val orgs = listOf(
    Organization(UUID.randomUUID(), "org1", 1, "org1", mutableListOf<Subscription>()),
    Organization(UUID.randomUUID(), "org2", 2, "org2", mutableListOf<Subscription>()),
    Organization(UUID.randomUUID(), "org3", 3, "org3", mutableListOf<Subscription>()),
    Organization(UUID.randomUUID(), "org4", 4, "org4", mutableListOf<Subscription>())
)

val users = listOf(
    User(UUID.randomUUID(), "one@user.org", "User", "One"),
    User(UUID.randomUUID(), "two@user.org", "User", "Two"),
    User(UUID.randomUUID(), "three@user.org", "User", "Three")
)

val keys = listOf(
    Key(UUID.randomUUID(), "desc1", users[0].userId!!, LocalDateTime.now(), true),
    Key(UUID.randomUUID(), "desc2", users[1].userId!!, LocalDateTime.now(), true),
    Key(UUID.randomUUID(), "desc3", users[2].userId!!, LocalDateTime.now(), true)
)

val staffTestUser = UserRepresentation().apply {
    username = "staff@crossref.org"
    firstName = "fred"
    lastName = "flintstone"
    email = "staff@crossref.org"
    isEnabled = true
    credentials = listOf(
        CredentialRepresentation().apply {
            value = "pwdPWD1!"
            type = "password"
        }
    )
}

val adminTestUser = UserRepresentation().apply {
    username = "crossref"
    firstName = "foo"
    lastName = "bar"
    email = "crossref@crossref.org"
    isEnabled = true
    credentials = listOf(
        CredentialRepresentation().apply {
            value = "pwdPWD1!"
            type = "password"
        }
    )
}

val testUser = UserRepresentation().apply {
    username = "user@crossref.org"
    firstName = "john"
    lastName = "smith"
    email = "user@crossref.org"
    isEnabled = true
    credentials = listOf(
        CredentialRepresentation().apply {
            value = "pwdPWD1!"
            type = "password"
        }
    )
}

val testUser2 = UserRepresentation().apply {
    username = "user2@crossref.org"
    firstName = "paul"
    lastName = "lennon"
    email = "user2@crossref.org"
    isEnabled = true
    credentials = listOf(
        CredentialRepresentation().apply {
            value = "pwdPWD1!"
            type = "password"
        }
    )
}

val rsaComponent = ComponentRepresentation().apply {
    name = "test_rsa"
    providerId = "rsa"
    providerType = "org.keycloak.keys.KeyProvider"
    config = MultivaluedHashMap<String, String>().apply {
        putSingle("priority", "101")
        putSingle("enabled", "true")
        putSingle("active", "true")
        putSingle("algorithm", "RS256")
        putSingle("privateKey", "-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEAr7f/gq62BTI0YZYzQwe7A7Mrf+gXiOngmUSJObuGTd4Mp+6FMJjAIRJCcav+epr59gJZDgrWg0bC6+eW5F7+/7bfU25HTAL7r9KyGToobV1mrxk/LhHbY+hHu/avPIXDKlE8vERrrAhEv0OFId6ouYP55bqitJ+l7bpoVTf3rqs182mvSCnVYqcgYiQNNddBLEahv7IeV6Uh9P+MglsWuDa2iUKuXT/9WVPCJlbyXSvtsQpVMjtW5pXKm5fGq8tIAkCF28XNFim2NQeTlVzIRQbXQSx5p2NxI96vOTnxstYfzGLlxwvlW5W219O2c3VQcpeL7TL9sEA0zwnkk9pDFQIDAQABAoIBAQCpgwDUGqKxRItPkMwG39BDc3aFP2pkw+nNMVaIASVAvPLMlcaa6OB/htPeQKOPty1na0Bp0X66yWqElLb1u8aW/N1X6MySwVX3eXQh7f7N8gp65AoU8VkJeiypYQIPiqKJkjDbFc4g+SEIuaQW1mtB+9T5DyCiylgRi3/WpdUkA6Buky0EQkNFoEQNObevccSBFzGOe9QJib/Fr1IMn0T/Qnc4+2yIkYOgmok6SL3ExJvJIyaIfVaz96AZJ5d8R2IBoU6/u6SrMurVfMUHmAmO1saUqIhlkgT8nfL9eK8SADZE1w0QM8AN2g9F3q6gKvzWnjFWW7NZf9Bo09rI3JmJAoGBAODz/pDdaKNJ28ELZJjBogM/UlfX34t1m4yuO0x5sb3BIL6YL3OJXLgUs3GIaM9yhCLUuo4QU+BAWHGie+mPBuXaCWs4N8QYzJR8n4XtCZFDZTgUtrBiQUA7BuhDGXdubRjTuNPbFr5om3RASfPDU6fcR5Dd09l+4AuUu/GTpDVDAoGBAMf4dqUB5AuSr57O9k9h9M+UrrI07ivRDDna+Ms/EdO+W74nRoDGJP8s8AdSmT17RJN8BG5mMulwh4dwVd7MIJt9cB+p70/hTeojmV7siLE9o7SWmAWtCwTmZsXnI1FmUkT+OxCjF7UJucSD/yt2V6/zQyDvKwNKyKns+KqCO/THAoGBAMxEAuachkWv7YJPzBsByRHVz/nAJldGheaq12HMJ1+9HPS00HHDS5REB+WSDkpBHEdcj8IshV1eUHFvL8GXhJX29RKNS+Ye9rXmrBw6b7OeVS0lcfacYc/nSZBiTZ+rgBlJN834ZaCk8HQRZJWayj3kwn+DTrlyghNCDZ+Bh1xtAoGBAMPr6cKNcbLtCugbejQYk1T2JZswRNHlRYdBxVTM/FhrP1O1A1yTxhfFHX7vLHiSV4PwlB7ePq98syy9oURi1ufBJKPBSVkIM+Bc9J2OPfb5n3UFvviArDUm0DbB2VadW6mBNLl+MXIMj1e8w7r/fbZELtcLvSbClIv4Q0I7yyZtAoGAPv2Ps4jtWeWK+IyI02kTAd63TtHQoVc7Qat2uXNnFmqhpoQtRAPAtXRBZeVuaIXtDbWGAW0bum+9d7Dh2smerjpzsLPqaY9k16FaM00mZKPNmRP8wX8y+6Qe5/nXf4j8JepENG+WCpWdfX1I+lAZehfoWVdL9wCstDlO3PxvQLg=\n-----END RSA PRIVATE KEY-----\n")
        putSingle("certificate", "-----BEGIN CERTIFICATE-----\nMIICnzCCAYcCBgGBzeXJajANBgkqhkiG9w0BAQsFADATMREwDwYDVQQDDAhDcm9zc3JlZjAeFw0yMjA3MDUxMDI0NTdaFw0zMjA3MDUxMDI2MzdaMBMxETAPBgNVBAMMCENyb3NzcmVmMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr7f/gq62BTI0YZYzQwe7A7Mrf+gXiOngmUSJObuGTd4Mp+6FMJjAIRJCcav+epr59gJZDgrWg0bC6+eW5F7+/7bfU25HTAL7r9KyGToobV1mrxk/LhHbY+hHu/avPIXDKlE8vERrrAhEv0OFId6ouYP55bqitJ+l7bpoVTf3rqs182mvSCnVYqcgYiQNNddBLEahv7IeV6Uh9P+MglsWuDa2iUKuXT/9WVPCJlbyXSvtsQpVMjtW5pXKm5fGq8tIAkCF28XNFim2NQeTlVzIRQbXQSx5p2NxI96vOTnxstYfzGLlxwvlW5W219O2c3VQcpeL7TL9sEA0zwnkk9pDFQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCpfLTXKgReHvcoEDVaEDRw6cBvjvokQA5onadIAK9pQBbtUJv+xb/oI4900EkZMmQN5i0vuvoN1K+HVZFwfIJ1710Hk/P4fgWtZxjinqO4GzBgd8K0vBmWxpV5U6RPKtsAWMaQneg6j/KRroMeYcl/MCiUqmqVoWrekQWrO61+MSKsX9hx0c9Ev76eb4dvtOfvze7VTPd9eCG+JLd+TYWV9Y/WqqxA0BGhT18GnC7ipYi3p8XoVflF3BR5+K0dB0ehw8D0g1bnBqWMDYF8Kct2j4ZbF0yJHj7EIWNjiFIFN5+itm0y1jNMfG5jfFHVXxC2BpTjEqCPDIxr9luQJNKN\n-----END CERTIFICATE-----\n")
    }
}

val rsaEncComponent = ComponentRepresentation().apply {
    name = "test_rsa-enc"
    providerId = "rsa-enc"
    providerType = "org.keycloak.keys.KeyProvider"
    config = MultivaluedHashMap<String, String>().apply {
        putSingle("priority", "101")
        putSingle("enabled", "true")
        putSingle("active", "true")
        putSingle("algorithm", "RSA-OAEP")
        putSingle("privateKey", "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAhbUdBZ9IgqwcJB+gfpXG7oTwjZ8JXNhQKs+elcMvdCSKSCidCnEbULzJ2/6u1aoPIdmDS407tU4qoK9waTvpRyGiy4Tym2CVC306jVGJPyJCiGnS6xYaCXd1YmSHXGrhYsiDSjZdq2sV4bHvXtJFumluA2HhdlB4JZwr3ZxLl0mHXAtIdSsgHa7jZ1XWZ8MHxWTlxJ8pnQhrX4ovbHtxALUWw9JkJjCCFFk1Li0OMur0gM1GClBQ7YK9peSa5tgrzjcpFNRgjihuZcwe/18jlsBNtezA409BPlBFLQf0/JLwdhD3qCJc2uC6V5CnfO8UQxadWFeG8/buF07WcS11XwIDAQABAoIBAHmKMdBrguktdAMuMKEAmy0z8Mp7d3JLO0w/IEO4gklL8ds399CHEjJo+gEdWGXmyQyXAS9LOaxMWploAIHYMOw02huwRSwSNkmUUwYkyOJ3zQzdTRnfJFnYmf6PrS/IBArcg+OQ2cK5e9GGAjYqY7HYpmUys1TuPtjJRmKS8W+ZzhilssJS8LLUtZ4X2RlftO3z7Pcas+D3VfBrhPYmmQG9t3hNUTsoee1B5U/OsYXQcrlPyKz2ROXe89ueAjVi4iT8ianq0nEbr8w8pz9mu3cECQUeEYmDnx5RgvzsKAUTUzJDGJq8yVwNZCLHI+dvX6VfHeisaxCs28d8gmVjCaECgYEA8Gl4aRT7FcCua6KCALgVGjpV3gZTMygQUltdasxk9PJ3vLiPqgF0/ntkk8N+zsSTaCjnBjs+HMyWv+DLdU1qJUQWP49moA9zFEFJFKQcSTB4Ta3mVJZx32cUaLHzFWJLZdDD0RNhxPltrzNoX/vdTkbV+TwIiSFMvYKgGklABE8CgYEAjmB8PuzKZuAdCc/L3eW32Dr1Z/aBlF6MqaDdDW5Fj24yyuBzztmSD/ssnNZOI22PXOToOveK9yvpGbQxhG8Bx6VFd1Hiq4DRW5HT4NLGvHuxOgjy2KTvqybupIZCEEW1R31114is5O9xOrGhU1lmfBUWWtxO6BvHTTqkHNlLafECgYEAoT7cV/6OeYSGeALDFyBgZvsBZKzxWLpiWBqXiW4VuIR38sgG84uOoLC6QLE6eAw4of/tE6zgeAhwg7mOPNrzepM0KOVRe8Fg+hDfp0x6/EvaJJ+igNF3BtPoSGAyHgGTeHKRI8XQKYDyJDRiqR4QzNON6GBviSoggmr+XyMKINkCgYAPeFpnt/DyJq3hEAwVy00jmJxrtHQ1++QK1mjOecnjINUyCBttQWTGicM6M2sAHHp0XIcDSTx3SPUmDtdGjY1C9SlMIsCfY/rbmUfNaPmhJ0uFg81nTtyf2zYY+GGbRQfCgdZZGge3RHEoMH7YAKHLH7tzGfcU71fDg3irjczM8QKBgCECO7uSbGxPaigYuRaFiTx7uv2jg9XAf4++j11GYKDYDVDwXGxKWwv1aELRlXbH8TaH+1/f4RFrywGh9cCyqk31pcWME4C93dVrtlCSvRGsjXkGymPf5HerVYiDzsFQ5PYVu77PxdyBkIycbFoMPbyL3QWdn/NkB7+b1sNPVpdR\n-----END RSA PRIVATE KEY-----\n")
        putSingle("certificate", "-----BEGIN CERTIFICATE-----\nMIICnzCCAYcCBgGBzeXJ9zANBgkqhkiG9w0BAQsFADATMREwDwYDVQQDDAhDcm9zc3JlZjAeFw0yMjA3MDUxMDI0NTdaFw0zMjA3MDUxMDI2MzdaMBMxETAPBgNVBAMMCENyb3NzcmVmMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhbUdBZ9IgqwcJB+gfpXG7oTwjZ8JXNhQKs+elcMvdCSKSCidCnEbULzJ2/6u1aoPIdmDS407tU4qoK9waTvpRyGiy4Tym2CVC306jVGJPyJCiGnS6xYaCXd1YmSHXGrhYsiDSjZdq2sV4bHvXtJFumluA2HhdlB4JZwr3ZxLl0mHXAtIdSsgHa7jZ1XWZ8MHxWTlxJ8pnQhrX4ovbHtxALUWw9JkJjCCFFk1Li0OMur0gM1GClBQ7YK9peSa5tgrzjcpFNRgjihuZcwe/18jlsBNtezA409BPlBFLQf0/JLwdhD3qCJc2uC6V5CnfO8UQxadWFeG8/buF07WcS11XwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCFWKaU/TXC+f9ZHsMTWXkxBOzMN4TddwIUOMiMiMbF5NKr1BnON79Wjh5jYE1t6cnVx5Eozb63myMuw7D+GJiTCdddn9KFY8yJ546sYEFGvqIC7PSpvcJ9LABzBDZEJqn5Tpc8ipMBOKEdMnUR/SH7kW3PgGbHsEcKxEeDBUvUJdhsNax52hnmDjUyc8lBy3wncpCMolSxVa4fFfUGezibQH0G4/VYZBECNPbxTAiU7jp3SDGLo3XFFuE7/i70/NcNxnggSF322/PeZ92Pxqu/tX55QTGy/6C+PLRIEcgA6dbFExUKKha5KAAltiK+qcchwWIBteF5IitCSNhk+HHD\n-----END CERTIFICATE-----\n")
    }
}

fun mockToken(vararg authorities: String): JwtRequestPostProcessor =
    jwt().jwt(Jwt.withTokenValue("asd").header("typ", "JWT").claim("sub", UUID.randomUUID()).build())
        .authorities(authorities.map { authority -> SimpleGrantedAuthority(authority) })

abstract class IntegrationTester {
    companion object {
        val serverUrl = System.getenv("KEYCLOAK_ADMIN_URL") ?: throw Exception("KEYCLOAK_ADMIN_URL not set")
        val authenticatorUrl = System.getenv("AUTH_URL") ?: throw Exception("AUTH_URL not set")
        val authenticatorUser = System.getenv("AUTH_SYNC_USER") ?: "test_internal_user"
        val authenticatorPwd = System.getenv("AUTH_SYNC_PWD") ?: "test_internal_password"

        fun getKeycloakToken(username: String, password: String): String =
            KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm("testrealm")
                .clientId("mycrossref")
                .username(username)
                .password(password)
                .build().tokenManager().accessTokenString

        val staffToken: String by lazy { getKeycloakToken("staff@crossref.org", "pwdPWD1!") }
        val userToken: String by lazy { getKeycloakToken("user@crossref.org", "pwdPWD1!") }
        val user2Token: String by lazy { getKeycloakToken("user2@crossref.org", "pwdPWD1!") }
        val syncToken: String by lazy { getKeycloakToken("sync", "pwdPWD1!") }
        val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())

        @BeforeAll
        @JvmStatic
        internal fun createRealm() {
            RealmSetup("testrealm", serverUrl, "admin", "pwdPWD1!", "pwdPWD1!").also {
                if (it.setUpRealm()) {
                    with(it.kcConn) {
                        orgs.forEach { this.addOrg(it) }

                        this.createKeycloakUser(staffTestUser, listOf("ROLE_STAFF"))
                        this.createKeycloakUser(testUser, listOf("ROLE_USER"))
                        this.createKeycloakUser(testUser2, listOf("ROLE_USER"))

                        this.addOrgAdminUser(
                            this.getOrgs().filter { it.name.equals("org1") }.first().orgId!!,
                            this.findUsers(
                                testUser.username, "", ""
                            ).first().userId!!
                        )

                        this.setRsaKeys(rsaComponent, rsaEncComponent)
                    }
                }
            }
        }
    }

    fun createAuthenticatorUser(username: String, password: String) {
        "${authenticatorUrl}api/test/adduser".httpPost()
            .authentication().basic(authenticatorUser, authenticatorPwd)
            .jsonBody(mapper.writeValueAsString(mapOf("username" to username, "password" to password)))
            .response()
    }

    fun setAuthenticatorUserPwd(username: String, password: String) {
        "${authenticatorUrl}api/test/setuserpwd".httpPost()
            .authentication().basic(authenticatorUser, authenticatorPwd)
            .jsonBody(mapper.writeValueAsString(mapOf("username" to username, "password" to password)))
            .response()
    }

    fun authenticatorUserExists(username: String) =
        "${authenticatorUrl}api/test/userexists/$username".httpGet()
            .authentication().basic(authenticatorUser, authenticatorPwd)
            .responseString().third.component1().equals("true")

    fun prepareReq(method: String, path: String, token: String, data: Any? = null) = with("http://localhost:9191$path") {
        when (method) {
            "GET" -> this.httpGet()
            "POST" -> this.httpPost()
            "PUT" -> this.httpPut()
            "DELETE" -> this.httpDelete()
            else -> throw Exception("method not supported")
        }.header("Authorization", "Bearer $token").also { if (data != null) { if (data is String) it.body(data) else it.jsonBody(mapper.writeValueAsString(data)) } }
    }

    fun execReq(method: String, path: String, token: String, data: Any? = null) =
        prepareReq(method, path, token, data).response().second

    fun checkStatus(p: Pair<Int, ResponseMessage>, s: HttpStatus, messagePattern: String? = null) {
        Assertions.assertEquals(s, p.second.status, "Wrong status message, it should be $s")
        Assertions.assertEquals(s.value(), p.first, "Wrong HTTP status code, it should be ${s.value()}")
        messagePattern?.let {
            Assertions.assertTrue(
                messagePattern.toRegex().matches(p.second.message),
                "Message [${p.second.message}] does not match regex `$messagePattern`"
            )
        }
    }
}
