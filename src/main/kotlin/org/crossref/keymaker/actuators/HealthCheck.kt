package org.crossref.keymaker.actuators

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator

class HealthCheck() : HealthIndicator {
    @Value("\${info.app.version:unknown}")
    private val version: String = ""

    @Value("\${env:dev}")
    private val env: String = ""

    override fun health(): Health {
        val builder = Health.up()

        return builder
            .withDetail("VER", version)
            .withDetail("ENV", env)
            .build()
    }
}
