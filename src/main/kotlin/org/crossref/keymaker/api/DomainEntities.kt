package org.crossref.keymaker.api

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.util.UUID

enum class Subscription {
    PLUS
}

data class Organization(
    @JsonProperty("orgId")
    var orgId: UUID?,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("sugarId")
    val sugarId: Long,
    @JsonProperty("description")
    val description: String?,
    @JsonProperty("subscriptions")
    val subscriptions: MutableList<Subscription> = mutableListOf()
)

data class User(
    @JsonProperty("userId")
    var userId: UUID?,
    @JsonProperty("username")
    val username: String,
    @JsonProperty("firstName")
    val firstName: String?,
    @JsonProperty("lastName")
    val lastName: String?
)

data class Key(
    @JsonProperty("keyId")
    var keyId: UUID,
    @JsonProperty("description")
    val description: String,
    @JsonProperty("issuedBy")
    val issuedBy: UUID,
    @JsonProperty("date")
    val date: LocalDateTime = LocalDateTime.now(),
    @JsonProperty("enabled")
    val enabled: Boolean = true
)

data class KeyToken(
    @JsonProperty("key")
    val key: Key,
    @JsonProperty("token")
    val token: String
)

data class HashSyncMessage(
    @JsonProperty("username")
    val username: String,
    @JsonProperty("salt")
    val salt: String,
    @JsonProperty("hash")
    val hash: String,
    @JsonProperty("algorithm")
    val algorithm: String,
    @JsonProperty("iterations")
    val iterations: Int
)

data class ResponseMessage(
    @JsonProperty("status")
    val status: HttpStatus,
    @JsonProperty("message")
    val message: String,
    @JsonProperty("errors")
    val errors: List<String?>
)
