package org.crossref.keymaker.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.crossref.keymaker.service.KeyAdminService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.util.UUID
import javax.annotation.security.RolesAllowed
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/keymaker/api/v1/key")
class KeyAdminController(
    val keyAdminService: KeyAdminService
) {
    @Operation(summary = "Add a key for an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Key created",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = KeyToken::class))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
        ]
    )
    @PostMapping("/{orgId}")
    @RolesAllowed("USER")
    @ResponseStatus(HttpStatus.CREATED)
    fun addKey(@PathVariable orgId: UUID, @RequestBody key: Key, request: HttpServletRequest): KeyToken =
        keyAdminService.addKey(orgId, UUID.fromString(request.userPrincipal.name), key)

    @Operation(summary = "Delete a key")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Key deleted"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Key not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @DeleteMapping("/{keyId}")
    @RolesAllowed("USER")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deletekey(@PathVariable keyId: UUID, request: HttpServletRequest) {
        keyAdminService.deleteKey(keyId, UUID.fromString(request.userPrincipal.name))
    }

    @Operation(summary = "Enable a key")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Key enabled"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "409", description = "Key already enabled", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @PutMapping("/{keyId}/enable")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun enableKey(@PathVariable keyId: UUID) {
        keyAdminService.setKeyEnabledStatus(keyId, true)
    }

    @Operation(summary = "Disable a key")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Key disabled"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "409", description = "Key already disabled", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @PutMapping("/{keyId}/disable")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun disableKey(@PathVariable keyId: UUID) {
        keyAdminService.setKeyEnabledStatus(keyId, false)
    }

    @Operation(summary = "Update a key")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Key updated"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Key not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @PutMapping("/{keyId}")
    @RolesAllowed("USER")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateKey(@PathVariable keyId: UUID, @RequestBody key: Key, request: HttpServletRequest) {
        key.keyId = keyId
        keyAdminService.updateKey(
            key,
            UUID.fromString(request.userPrincipal.name)
        )
    }
}
