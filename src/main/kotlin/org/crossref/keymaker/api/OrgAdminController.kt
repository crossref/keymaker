package org.crossref.keymaker.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.crossref.keymaker.service.KeyAdminService
import org.crossref.keymaker.service.OrgAdminService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import java.util.UUID
import javax.annotation.security.RolesAllowed
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/keymaker/api/v1/organization")
class OrgAdminController(
    val orgAdminService: OrgAdminService,
    val keyAdminService: KeyAdminService
) {
    @Operation(summary = "Get all organizations")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Found organizations",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = Organization::class))))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
        ]
    )
    @GetMapping
    @RolesAllowed("USER")
    fun findOrgs(@RequestParam searchParams: Map<String, String>, request: HttpServletRequest): List<Organization> {
        return if (request.isUserInRole("STAFF"))
            orgAdminService.findOrgs(searchParams)
        else orgAdminService.findOrgs(searchParams, UUID.fromString(request.userPrincipal.name))
    }

    @Operation(summary = "Get an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Found organization",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = Organization::class))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @GetMapping("/{orgId}")
    @RolesAllowed("STAFF")
    fun getOrg(@PathVariable("orgId") orgId: UUID): Organization = orgAdminService.getOrg(orgId)

    @Operation(summary = "Create an organization")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Organization created",
                content = [ (Content(mediaType = "application/json", schema = Schema(implementation = Organization::class))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "409", description = "Organization already exists", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE])
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.CREATED)
    fun addOrg(@RequestBody organization: Organization): Organization = orgAdminService.addOrg(organization)

    @Operation(summary = "Update an organization")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Organization created"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
            ApiResponse(responseCode = "409", description = "Organization already exists", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))]),
        ]
    )
    @PutMapping(value = ["/{orgId}"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateOrg(@PathVariable orgId: UUID, @RequestBody organization: Organization) =
        orgAdminService.updateOrg(organization.apply { this.orgId = orgId })

    @Operation(summary = "Delete an organization")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Organization deleted"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @DeleteMapping(value = ["/{orgId}"])
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteOrg(@PathVariable orgId: UUID) =
        orgAdminService.deleteOrg(orgId)

    @Operation(summary = "Get organization users")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Found users",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = User::class))))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @GetMapping("/{orgId}/users")
    @RolesAllowed("STAFF")
    fun getOrgUsers(@PathVariable("orgId") orgId: UUID): List<User> =
        orgAdminService.getOrgUsers(orgId)

    @Operation(summary = "Get organization user admins")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Found user admins",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = User::class))))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @GetMapping("/{orgId}/admins")
    @RolesAllowed("STAFF")
    fun getOrgAdmins(@PathVariable("orgId") orgId: UUID): List<User> =
        orgAdminService.getOrgAdmins(orgId)

    @Operation(summary = "Add user admin to organization")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "User admin added to organization"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization or user not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @PostMapping("/{orgId}/admins/{userId}")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun addOrgAdminUser(@PathVariable orgId: UUID, @PathVariable userId: UUID) =
        orgAdminService.addOrgAdminUser(orgId, userId)

    @Operation(summary = "Remove user admin from organization")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "User admin removed from organization"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization or user not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @DeleteMapping("/{orgId}/admins/{userId}")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun removeOrgAdminUser(@PathVariable orgId: UUID, @PathVariable userId: UUID) =
        orgAdminService.removeOrgAdminUser(orgId, userId)

    @Operation(summary = "Enable subscription for an organization")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Subscription enabled for organization"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @PutMapping("/{orgId}/subscription/{subscription}/enable")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun enableOrgSubscription(@PathVariable orgId: UUID, @PathVariable subscription: Subscription) =
        orgAdminService.setOrgSubscriptionStatus(orgId, subscription, true)

    @Operation(summary = "Disable subscription for an organization")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "204", description = "Subscription disabled for organization"),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
            ApiResponse(responseCode = "404", description = "Organization not found", content = [Content(mediaType = "application/json", schema = Schema(implementation = ResponseMessage::class))])
        ]
    )
    @PutMapping("/{orgId}/subscription/{subscription}/disable")
    @RolesAllowed("STAFF")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun disableOrgSubscription(@PathVariable orgId: UUID, @PathVariable subscription: Subscription) =
        orgAdminService.setOrgSubscriptionStatus(orgId, subscription, false)

    @Operation(summary = "Get Organization keys")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Found keys",
                content = [ (Content(mediaType = "application/json", array = (ArraySchema(schema = Schema(implementation = Key::class))))) ]
            ),
            ApiResponse(responseCode = "401", description = "Unauthorized", content = [Content()]),
        ]
    )
    @GetMapping("/{orgId}/keys")
    @RolesAllowed("USER")
    fun getOrgKeys(@PathVariable orgId: UUID, request: HttpServletRequest): List<Key> =
        if (request.isUserInRole("STAFF"))
            keyAdminService.findKeys(orgId)
        else keyAdminService.findKeys(orgId, UUID.fromString(request.userPrincipal.name))
}
