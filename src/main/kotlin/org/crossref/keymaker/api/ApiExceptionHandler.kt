package org.crossref.keymaker.api

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ApiExceptionHandler {
    @ExceptionHandler(AuthServiceUnavailableException::class)
    fun handleAuthServiceUnavailable(ex: AuthServiceUnavailableException): ResponseEntity<Any> =
        responseEntity(HttpStatus.SERVICE_UNAVAILABLE, "Authorization service unavailable", ex.message)

    @ExceptionHandler(OrgNotFoundException::class)
    fun handleOrgNotFound(ex: OrgNotFoundException): ResponseEntity<Any> =
        responseEntity(HttpStatus.NOT_FOUND, "Organization with ID ${ex.orgId} not found")

    @ExceptionHandler(OrgAlreadyExistsException::class)
    fun handleOrgAlreadyExists(ex: OrgAlreadyExistsException): ResponseEntity<Any> =
        responseEntity(
            HttpStatus.CONFLICT,
            "Organization with name ${ex.name} and / or Sugar ID ${ex.sugarId} already exists with ID ${ex.orgId}"
        )

    @ExceptionHandler(UserAlreadyExistsException::class)
    fun handleUserAlreadyExists(ex: UserAlreadyExistsException): ResponseEntity<Any> =
        responseEntity(
            HttpStatus.CONFLICT,
            "User with username ${ex.username} already exists with ID ${ex.userId}"
        )

    @ExceptionHandler(UserNotFoundException::class)
    fun handleUserNotFound(ex: UserNotFoundException): ResponseEntity<Any> =
        responseEntity(HttpStatus.NOT_FOUND, "User with ID ${ex.userId} not found")

    @ExceptionHandler(UsernameNotFoundException::class)
    fun handleUsernameNotFound(ex: UsernameNotFoundException): ResponseEntity<Any> =
        responseEntity(HttpStatus.NOT_FOUND, "Username ${ex.username} not found")

    @ExceptionHandler(KeyNotFoundException::class)
    fun handleKeyNotFound(ex: KeyNotFoundException): ResponseEntity<Any> =
        responseEntity(HttpStatus.NOT_FOUND, "Key with ID ${ex.keyId} not found")

    @ExceptionHandler(InvalidUsernameException::class)
    fun handleInvalidUsername(ex: InvalidUsernameException): ResponseEntity<Any> =
        responseEntity(HttpStatus.BAD_REQUEST, "Username must be a valid email address")

    @ExceptionHandler(InvalidOrgNameException::class)
    fun handleInvalidOrgName(ex: InvalidOrgNameException): ResponseEntity<Any> =
        responseEntity(HttpStatus.BAD_REQUEST, "Organization name contains invalid characters")

    @ExceptionHandler(UserAlreadyOrgAdminException::class)
    fun handleUserAlreadyOrgAdmin(ex: UserAlreadyOrgAdminException): ResponseEntity<Any> =
        responseEntity(
            HttpStatus.CONFLICT,
            "User with ID ${ex.userId} is already an administrator of organization with ID ${ex.orgId}"
        )

    @ExceptionHandler(UserNotOrgAdminException::class)
    fun handleUserNotOrgAdmin(ex: UserNotOrgAdminException): ResponseEntity<Any> =
        responseEntity(
            HttpStatus.CONFLICT,
            "User with ID ${ex.userId} is not an administrator of organization with ID ${ex.orgId}"
        )

    @ExceptionHandler(OrgSubscriptionUnchangedException::class)
    fun handleOrgSubscriptionUnchanged(ex: OrgSubscriptionUnchangedException): ResponseEntity<Any> =
        responseEntity(
            HttpStatus.CONFLICT,
            "Organization with ID ${ex.orgId} is already " + if (ex.status) {
                "subscribed to"
            } else {
                "unsubscribed from"
            } + " ${ex.subscription}"
        )

    @ExceptionHandler(KeyStatusUnchangedException::class)
    fun handleKeyStatusUnchanged(ex: KeyStatusUnchangedException): ResponseEntity<Any> =
        responseEntity(
            HttpStatus.CONFLICT,
            "Key with ID ${ex.keyId} is already ${if (ex.status) "enabled" else "disabled"}"
        )

    @ExceptionHandler(org.springframework.security.access.AccessDeniedException::class)
    fun handleAccessDenied(ex: org.springframework.security.access.AccessDeniedException) =
        responseEntity(HttpStatus.UNAUTHORIZED, "Client is not authorized to access this resource")

    @ExceptionHandler(Exception::class)
    fun handleOther(ex: Exception): ResponseEntity<Any> =
        responseEntity(HttpStatus.INTERNAL_SERVER_ERROR, "An unknown error occurred", ex.message)

    private fun responseEntity(httpStatus: HttpStatus, message: String): ResponseEntity<Any> =
        responseEntity(httpStatus, message, emptyList())

    private fun responseEntity(httpStatus: HttpStatus, message: String, error: String?): ResponseEntity<Any> =
        responseEntity(httpStatus, message, listOf(error))

    private fun responseEntity(httpStatus: HttpStatus, message: String, errors: List<String?>): ResponseEntity<Any> =
        ResponseEntity(
            ResponseMessage(httpStatus, message, errors),
            HttpHeaders(),
            httpStatus
        )
}
