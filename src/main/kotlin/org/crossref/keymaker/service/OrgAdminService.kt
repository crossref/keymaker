package org.crossref.keymaker.service

import org.crossref.keymaker.api.Organization
import org.crossref.keymaker.api.Subscription
import org.crossref.keymaker.api.User
import java.util.UUID

interface OrgAdminService {
    fun findOrgs(searchParams: Map<String, String>): List<Organization>

    fun findOrgs(searchParams: Map<String, String>, userId: UUID): List<Organization>

    fun getOrg(orgId: UUID): Organization

    fun addOrg(organization: Organization): Organization

    fun updateOrg(organization: Organization)

    fun deleteOrg(orgId: UUID)

    fun addOrgAdminUser(orgId: UUID, userId: UUID)

    fun removeOrgAdminUser(orgId: UUID, userId: UUID)

    fun setOrgSubscriptionStatus(orgId: UUID, subscription: Subscription, status: Boolean)

    fun getOrgUsers(orgId: UUID): List<User>

    fun getOrgAdmins(orgId: UUID): List<User>
}
