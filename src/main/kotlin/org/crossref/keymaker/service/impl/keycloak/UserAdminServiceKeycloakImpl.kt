package org.crossref.keymaker.service.impl.keycloak

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpPost
import io.sentry.Sentry
import org.crossref.keymaker.api.User
import org.crossref.keymaker.service.UserAdminService
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class UserAdminServiceKeycloakImpl(private val kc: KeycloakConnector) : UserAdminService {
    @Value("\${config.authenticator.url}")
    private val authenticatorUrl: String = ""
    @Value("\${config.authenticator.user}")
    private val authenticatorUser: String = ""
    @Value("\${config.authenticator.pwd}")
    private val authenticatorPwd: String = ""

    private val mapper = jacksonObjectMapper()

    override fun findUsers(username: String?, firstName: String?, lastName: String?): List<User> {
        return kc.findUsers(
            username ?: "",
            firstName ?: "",
            lastName ?: ""
        )
    }

    override fun addUser(user: User): User {
        val user = kc.addUser(user)
        try {
            if (authenticatorUrl
                .httpPost()
                .authentication()
                .basic(authenticatorUser, authenticatorPwd)
                .jsonBody(mapper.writeValueAsString(mapOf("username" to user.username)))
                .response()
                .second
                .statusCode != 200
            ) {
                throw Exception("Authenticator sync failed for user ${user.username}")
            }
        } catch (e: Exception) {
            Sentry.captureException(e)
        }

        return user
    }

    override fun getUser(userId: UUID): User = kc.getUser(userId)

    override fun updateUser(user: User) {
        kc.updateUser(user)
    }

    override fun deleteUser(userId: UUID) {
        kc.deleteUser(userId)
    }
}
