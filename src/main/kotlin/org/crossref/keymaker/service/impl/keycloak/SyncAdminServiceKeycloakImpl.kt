package org.crossref.keymaker.service.impl.keycloak

import org.crossref.keymaker.service.SyncAdminService
import org.springframework.stereotype.Service

@Service
class SyncAdminServiceKeycloakImpl(val kc: KeycloakConnector) : SyncAdminService {
    override fun syncPassword(username: String, hash: String, salt: String, algorithm: String, iterations: Int) {
        kc.updateUserPasswordHash(username, hash, salt, algorithm, iterations)
    }
}
