package org.crossref.keymaker.service.impl.keycloak

import org.crossref.keymaker.api.Organization
import org.crossref.keymaker.api.Subscription
import org.crossref.keymaker.api.User
import org.crossref.keymaker.service.OrgAdminService
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class OrgAdminServiceKeycloakImpl(val kc: KeycloakConnector) : OrgAdminService {

    override fun findOrgs(searchParams: Map<String, String>): List<Organization> {
        val orgs = kc.getOrgs()
            .filter { it.name.lowercase().contains(searchParams.getOrDefault("name", "").lowercase()) }

        val subscription = searchParams.getOrDefault("subscription", null)
        if (subscription != null) {
            return orgs.filter { it.subscriptions.contains(Subscription.valueOf(subscription)) }
        }
        return orgs
    }

    override fun findOrgs(searchParams: Map<String, String>, userId: UUID): List<Organization> = kc.getUserOrgs(userId)

    override fun addOrg(organization: Organization): Organization = kc.addOrg(organization)

    override fun updateOrg(organization: Organization) = kc.updateOrg(organization)

    override fun deleteOrg(orgId: UUID) {
        kc.deleteOrg(orgId)
    }

    override fun addOrgAdminUser(orgId: UUID, userId: UUID) {
        kc.addOrgAdminUser(orgId, userId)
    }

    override fun removeOrgAdminUser(orgId: UUID, userId: UUID) {
        kc.removeOrgAdminUser(orgId, userId)
    }

    override fun setOrgSubscriptionStatus(orgId: UUID, subscription: Subscription, status: Boolean) {
        kc.setOrgSubscriptionStatus(orgId, subscription, status)
    }

    override fun getOrg(orgId: UUID): Organization = kc.getOrg(orgId)

    override fun getOrgUsers(orgId: UUID): List<User> = kc.getUsersForOrg(orgId)

    override fun getOrgAdmins(orgId: UUID): List<User> = kc.getAdminsForOrg(orgId)
}
