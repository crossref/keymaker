package org.crossref.keymaker.service.impl.keycloak

import org.crossref.keymaker.api.Key
import org.crossref.keymaker.api.KeyNotFoundException
import org.crossref.keymaker.api.KeyStatusUnchangedException
import org.crossref.keymaker.api.KeyToken
import org.crossref.keymaker.api.UserNotFoundException
import org.crossref.keymaker.service.KeyAdminService
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class KeyAdminServiceKeycloakImpl(val kc: KeycloakConnector) : KeyAdminService {

    override fun findKeys(orgId: UUID): List<Key> = kc.getKeysForOrg(orgId)

    override fun findKeys(orgId: UUID, userId: UUID): List<Key> {
        kc.checkUserIsGroupAdmin(orgId, userId)
        return findKeys(orgId)
    }

    override fun addKey(orgId: UUID, userId: UUID, key: Key): KeyToken {
        kc.checkUserIsGroupAdmin(orgId, userId)
        return kc.createKey(orgId, userId, key.description)
    }

    override fun deleteKey(keyId: UUID, userId: UUID) {
        val org = try {
            kc.getUserOrgs(keyId)[0]
        } catch (e: UserNotFoundException) {
            throw KeyNotFoundException(keyId)
        }
        org.orgId?.let {
            kc.checkUserIsGroupAdmin(it, userId)
            kc.deleteKey(keyId)
        }
    }

    override fun setKeyEnabledStatus(keyId: UUID, enabled: Boolean) {
        kc.getKey(keyId).enabled == enabled && throw KeyStatusUnchangedException(keyId, enabled)
        kc.setKeyEnabled(keyId, enabled)
    }

    override fun updateKey(key: Key, userId: UUID) {
        kc.getKeyOrg(key.keyId).orgId?.let {
            kc.checkUserIsGroupAdmin(it, userId)
            kc.updateKey(key)
        }
    }
}
