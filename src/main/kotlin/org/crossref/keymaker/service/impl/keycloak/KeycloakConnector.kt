package org.crossref.keymaker.service.impl.keycloak

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.common.hash.Hashing
import org.apache.commons.codec.binary.Base32
import org.apache.commons.validator.routines.EmailValidator
import org.crossref.keymaker.api.AuthServiceUnavailableException
import org.crossref.keymaker.api.InvalidOrgNameException
import org.crossref.keymaker.api.InvalidUsernameException
import org.crossref.keymaker.api.Key
import org.crossref.keymaker.api.KeyNotFoundException
import org.crossref.keymaker.api.KeyStatusUnchangedException
import org.crossref.keymaker.api.KeyToken
import org.crossref.keymaker.api.OrgAlreadyExistsException
import org.crossref.keymaker.api.OrgNotFoundException
import org.crossref.keymaker.api.OrgSubscriptionUnchangedException
import org.crossref.keymaker.api.Organization
import org.crossref.keymaker.api.Subscription
import org.crossref.keymaker.api.User
import org.crossref.keymaker.api.UserAlreadyExistsException
import org.crossref.keymaker.api.UserAlreadyOrgAdminException
import org.crossref.keymaker.api.UserNotFoundException
import org.crossref.keymaker.api.UserNotOrgAdminException
import org.crossref.keymaker.api.UsernameNotFoundException
import org.crossref.keymaker.config.subscriptionToResource
import org.keycloak.admin.client.CreatedResponseUtil
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.admin.client.resource.PolicyResource
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.ComponentRepresentation
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.GroupRepresentation
import org.keycloak.representations.idm.RealmRepresentation
import org.keycloak.representations.idm.RoleRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.keycloak.representations.idm.authorization.DecisionStrategy
import org.keycloak.representations.idm.authorization.Logic
import org.keycloak.representations.idm.authorization.PolicyRepresentation
import org.keycloak.representations.idm.authorization.ResourcePermissionRepresentation
import org.keycloak.representations.idm.authorization.ResourceRepresentation
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.UUID
import javax.ws.rs.NotFoundException
import javax.ws.rs.ProcessingException
import javax.ws.rs.core.Response

class KeycloakConnector(val url: String, val realm: String, user: String, password: String) {
    private val keycloak: Keycloak
    lateinit var realmResource: RealmResource
    private val mapper = jacksonObjectMapper()
    val emailValidator: EmailValidator = EmailValidator.getInstance()
    val logger: Logger = LoggerFactory.getLogger(this::class.java)!!

    init {
        keycloak = KeycloakBuilder.builder()
            .serverUrl(url)
            .realm(realm)
            .clientId("admin-cli")
            .username(user)
            .password(password)
            .build()
        setRealm(realm)
    }

    fun setRealm(name: String) {
        realmResource = keycloak.realm(name)
    }

    fun getRealms() = keycloak.realms().findAll().map { it.realm }

    fun createRealm(name: String) {
        with(RealmRepresentation()) {
            realm = name
            isEnabled = true
            keycloak.realms().create(this)
        }
    }

    fun createClient(cr: ClientRepresentation) {
        realmResource.clients().create(cr)
    }

    fun setRealmEventsLogging(enabled: Boolean) {
        realmResource.updateRealmEventsConfig(realmResource.realmEventsConfig.apply { isEventsEnabled = enabled })
    }

    fun setRealmPasswordPolicy(policy: String) {
        realmResource.update(realmResource.toRepresentation().apply { passwordPolicy = policy })
    }

    fun getRoleRepresentation(name: String) = realmResource.roles().get(name).toRepresentation()

    fun getPolicyForResource(clientName: String, resourceName: String): Pair<PolicyResource, PolicyRepresentation> =
        realmResource.clients().findByClientId(clientName).first().id.let { clientId ->
            realmResource.clients().get(clientId).authorization().policies().findByName("$resourceName-access-policy").let {
                Pair(realmResource.clients().get(clientId).authorization().policies().policy(it.id), it)
            }
        }

    fun addReadOnlyGroupResource(clientId: String, resourceName: String, path: String) {
        val baseOrgsId = getOrgsGroup().id

        realmResource.clients().findByClientId(clientId).first().id.also { clientUUID ->
            ResourceRepresentation().apply {
                name = resourceName
                displayName = "$resourceName resource"
                uris = setOf(path)
            }.also {
                realmResource.clients().get(clientUUID).authorization().resources().create(it)
            }
            PolicyRepresentation().apply {
                type = "group"
                name = "$resourceName-access-policy"
                logic = Logic.POSITIVE
                decisionStrategy = DecisionStrategy.UNANIMOUS
                config = linkedMapOf(
                    "groups" to mapper.writeValueAsString(
                        listOf(
                            mapOf<String, Any>(
                                "id" to baseOrgsId,
                                "extendChildren" to false
                            )
                        )
                    )
                )
            }.also {
                realmResource.clients().get(clientUUID).authorization().policies().create(it)
            }
            ResourcePermissionRepresentation().apply {
                name = "$resourceName-permission"
                policies = setOf(realmResource.clients().get(clientUUID).authorization().policies().findByName("$resourceName-access-policy").id)
                resources = setOf(realmResource.clients().get(clientUUID).authorization().resources().findByName(resourceName).first().id)
            }.also {
                realmResource.clients().get(clientUUID).authorization().permissions().resource().create(it)
            }
        }
    }

    fun UserRepresentation.toKey(): Key {
        return Key(
            UUID.fromString(this.id),
            this.attributes.getOrDefault("description", listOf(""))[0],
            UUID.fromString(this.attributes.get("issuedBy")!![0]),
            LocalDateTime.ofEpochSecond(this.createdTimestamp / 1000, 0, ZoneOffset.UTC),
            this.isEnabled
        )
    }

    fun UserRepresentation.toUser(): User {
        return User(UUID.fromString(this.id), this.username, this.firstName, this.lastName)
    }

    fun GroupRepresentation.toOrg(): Organization {
        val sugarId = (this.attributes?.get("sugarId") ?: listOf("-1"))[0]
        val description = (this.attributes?.get("description") ?: listOf(""))[0]
        val subscriptions = (this.attributes?.get("subscriptions") ?: listOf("[]"))[0]

        return Organization(
            UUID.fromString(this.id),
            this.name,
            sugarId.toLong(),
            description,
            mapper.readValue(subscriptions, List::class.java)
                .map { Subscription.valueOf(it.toString()) } as MutableList<Subscription>
        )
    }

    fun Organization.toGroupRepresentation(): GroupRepresentation {
        val grp = GroupRepresentation()
        grp.name = this.name
        grp.id = this.orgId.toString()
        grp.attributes = mutableMapOf<String, List<String>>()
        grp.attributes.set("sugarId", listOf("${this.sugarId}"))
        grp.attributes["description"] = listOf<String>("${this.description ?: ""}")
        grp.attributes["subscriptions"] = listOf(mapper.writeValueAsString(this.subscriptions))
        return grp
    }

    fun Organization.members() =
        realmResource.groups().group(this.orgId.toString()).members()

    /*
     KEY MANAGEMENT
     */

    fun generateApiKey(): String {
        var key = ""
        for (i in 1..3) {
            key += Base32().encodeAsString(UUID.randomUUID().toString().replace("-", "").toBigInteger(16).toByteArray())
                .replace("=", "")
        }

        return "x${key.slice(0..19)}r${key.slice(20..39)}e${key.slice(40..59)}f".lowercase()
    }

    fun createKey(orgId: UUID, userId: UUID, description: String): KeyToken {
        val key = generateApiKey()
        val orgName: String

        try {
            orgName = realmResource
                .groups()
                .group(orgId.toString())
                .toRepresentation()
                .name
                .replace(Regex("[^a-z0-9]", RegexOption.IGNORE_CASE), "")
        } catch (e: NotFoundException) {
            throw OrgNotFoundException(orgId)
        }

        UserRepresentation().also {
            val username = "apikey_${orgName}_${UUID.randomUUID().toString().slice(0..7)}"
            it.username = username
            it.isEnabled = true
            it.attributes = mutableMapOf<String, List<String>>()
            it.attributes["issuedBy"] = mutableListOf(userId.toString())
            it.attributes["description"] = mutableListOf(description)
            it.attributes["api-key"] = mutableListOf(
                Hashing.sha256()
                    .hashString((System.getenv("APIKEY_SALT") ?: "") + key, Charsets.US_ASCII)
                    .toString()
                    .lowercase()
            )

            val response = realmResource.users().create(it)
            if (response.status != 201) {
                throw Exception(response.statusInfo.reasonPhrase)
            }

            val keyId = CreatedResponseUtil.getCreatedId(response)
            val keyResource = realmResource.users().get(keyId)
                .also {
                    it.update(
                        with(it.toRepresentation()) {
                            this.username = "apikey_$keyId"
                            this
                        }
                    )
                }

            keyResource.roles().realmLevel().add(listOf(getRoleRepresentation("ROLE_APIKEY")))

            try {
                keyResource.joinGroup(orgId.toString())
            } catch (e: NotFoundException) {
                keyResource.remove()
                throw OrgNotFoundException(orgId)
            }

            return KeyToken(getKey(UUID.fromString(keyId)), key)
        }
    }

    fun setKeyEnabled(keyId: UUID, enabledStatus: Boolean) {
        getKeyRepresentation(keyId).also {
            it.isEnabled == enabledStatus && throw KeyStatusUnchangedException(keyId, enabledStatus)
            it.isEnabled = enabledStatus
            realmResource.users().get(keyId.toString()).update(it)
        }
    }

    fun getKeysForOrg(orgId: UUID) = getOrg(orgId)
        .members()
        .filter { it.username.startsWith("apikey_") }
        .map { it.toKey() }

    fun updateKey(key: Key) {
        key.keyId.let {
            getKeyRepresentation(key.keyId).also {
                it.attributes["description"] = mutableListOf(key.description)
                realmResource.users().get(key.keyId.toString()).update(it)
            }
        }
    }

    fun getKeyRepresentation(userId: UUID) =
        try {
            getUserRepresentation(userId)
        } catch (e: UserNotFoundException) {
            throw KeyNotFoundException(userId)
        }

    fun getKey(userId: UUID) =
        getKeyRepresentation(userId).toKey()

    fun deleteKey(keyId: UUID) {
        try {
            deleteUser(keyId)
        } catch (e: UserNotFoundException) {
            throw KeyNotFoundException(keyId)
        }
    }

    /*
      ORGANIZATION MANAGEMENT
     */

    fun getOrgs() = getOrgsGroup().subGroups.map { it.toOrg() }

    fun getOrgRepresentation(orgId: UUID) =
        try {
            realmResource.groups().group(orgId.toString()).toRepresentation()
        } catch (e: NotFoundException) {
            throw OrgNotFoundException(orgId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }

    fun getOrg(orgId: UUID) =
        getOrgRepresentation(orgId).toOrg()

    fun getUsersForOrg(orgId: UUID) = getOrg(orgId)
        .members()
        .map { it.toUser() }
        .filterNot { it.username.startsWith("apikey_") }

    fun getAdminsForOrg(orgId: UUID): List<User> =
        getGroupAdminId(orgId).let {
            getOrg(UUID.fromString(it)).members().map { it.toUser() }
        }

    private fun getOrgsGroup(): GroupRepresentation {
        return try {
            realmResource.getGroupByPath("/organizations")
        } catch (e: NotFoundException) {
            throw Exception("Group /organizations not found")
        }
    }

    fun addOrg(organization: Organization): Organization {
        "[a-z0-9_&()\\[\\]., -]+".toRegex().matches(organization.name) || throw InvalidOrgNameException()

        val grp = organization.toGroupRepresentation()
        grp.path = "/organizations/" + grp.name
        grp.id = null

        try {
            val g = realmResource.getGroupByPath(grp.path)
            throw OrgAlreadyExistsException(UUID.fromString(g.id), g.name, g.attributes.getOrDefault("sugarId", listOf("-1"))[0].toLong())
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        } catch (e: NotFoundException) { /* Empty because we only continue if the org has not been found */ }

        val adminGroup = GroupRepresentation()
        adminGroup.name = "admins"

        grp.id = addSubGroup(getOrgsGroup(), grp).toString()
        addSubGroup(grp, adminGroup)

        return grp.toOrg()
    }

    fun updateOrg(organization: Organization) {
        val grp = organization.toGroupRepresentation()
        organization.orgId?.let {
            val origGroup = realmResource.groups().group(it.toString())
            try {
                grp.attributes["subscriptions"] =
                    origGroup.toRepresentation().attributes.getOrDefault("subscriptions", listOf("[]"))
            } catch (e: NotFoundException) {
                throw OrgNotFoundException(it)
            } catch (e: ProcessingException) {
                throw AuthServiceUnavailableException(e.message)
            }
            origGroup.update(grp)
        }
    }

    fun deleteOrg(orgId: UUID) {
        try {
            realmResource.groups().group(orgId.toString()).remove()
        } catch (e: NotFoundException) {
            throw OrgNotFoundException(orgId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun addOrgAdminUser(orgId: UUID, userId: UUID) {
        val adminsGroupId = getGroupAdminId(orgId)

        val user = realmResource.users().get(userId.toString())

        try {
            user.groups().any { it.id == adminsGroupId.toString() } && throw UserAlreadyOrgAdminException(orgId, userId)
            user.joinGroup(orgId.toString())
            user.joinGroup(adminsGroupId.toString())
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun removeOrgAdminUser(orgId: UUID, userId: UUID) {
        val adminsGroupId = getGroupAdminId(orgId)

        try {
            val user = realmResource.users().get(userId.toString())
            user.groups().all { it.id != adminsGroupId.toString() } && throw UserNotOrgAdminException(orgId, userId)
            user.leaveGroup(orgId.toString())
            user.leaveGroup(adminsGroupId.toString())
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun addGroupToPolicy(groupId: UUID, policyResource: PolicyResource, policyRepresentation: PolicyRepresentation) {
        val groups: MutableList<Map<String, Any>> = mapper.readValue(policyRepresentation.config.getOrDefault("groups", "[]"))
        if (groups.none { it.getOrDefault("id", "").equals(groupId.toString()) }) {
            groups.add(mapOf<String, Any>("id" to groupId.toString(), "extendChildren" to false))
            policyRepresentation.config["groups"] = mapper.writeValueAsString(groups)
            policyResource.update(policyRepresentation)
        }
    }

    fun removeGroupFromPolicy(groupId: UUID, policyResource: PolicyResource, policyRepresentation: PolicyRepresentation) {
        val groups: MutableList<Map<String, Any>> = mapper.readValue(policyRepresentation.config.getOrDefault("groups", "[]"))
        if (groups.any { it.getOrDefault("id", "").equals(groupId.toString()) }) {
            policyRepresentation.config["groups"] = mapper.writeValueAsString(groups.filterNot { it.getOrDefault("id", "").equals(groupId.toString()) })
            policyResource.update(policyRepresentation)
        }
    }

    fun setOrgSubscriptionStatus(orgId: UUID, subscription: Subscription, status: Boolean) {
        val policies = subscriptionToResource[subscription]?.map { (clientName, resources) ->
            resources.map { getPolicyForResource(clientName, it) }
        }?.flatten()

        val org = getOrg(orgId)
        if (status && !org.subscriptions.contains(subscription)) {
            org.subscriptions.add(subscription)
            policies?.forEach { (policyResource, policyRepresentation) ->
                addGroupToPolicy(orgId, policyResource, policyRepresentation)
            }
        } else if (!status && org.subscriptions.contains(subscription)) {
            org.subscriptions.remove(subscription)
            policies?.forEach { (policyResource, policyRepresentation) ->
                removeGroupFromPolicy(orgId, policyResource, policyRepresentation)
            }
        } else
            throw OrgSubscriptionUnchangedException(orgId, subscription, status)

        val grp = org.toGroupRepresentation()
        org.orgId?.let { realmResource.groups().group(it.toString()).update(grp) }
    }

    /*
    USER MANAGEMENT
    */

    fun findUsers(username: String, firstName: String, lastName: String): List<User> =
        realmResource.users().search(username, firstName, lastName, "", 0, 1000).map { it.toUser() }

    fun createKeycloakUser(u: UserRepresentation, roles: List<String>? = null, password: String? = null): String {
        password?.let {
            u.credentials = listOf(
                CredentialRepresentation().apply {
                    value = password
                    type = "password"
                }
            )
        }

        val response = realmResource.users().create(u)

        if (response.status != 201) {
            throw Exception(response.statusInfo.reasonPhrase)
        }

        val id = CreatedResponseUtil.getCreatedId(response)

        roles?.let {
            val userResource = realmResource.users().get(id)
            userResource.roles().realmLevel().add(roles.map { getRoleRepresentation(it) })
        }

        return id
    }

    fun addUser(user: User): User {
        val ur = UserRepresentation()

        if (!emailValidator.isValid(user.username)) {
            throw InvalidUsernameException()
        }

        val existingUser = realmResource.users().search(user.username).filter { it.username == user.username }
        if (existingUser.isNotEmpty()) {
            throw UserAlreadyExistsException(UUID.fromString(existingUser[0].id), existingUser[0].username)
        }

        ur.username = user.username
        ur.email = user.username
        ur.firstName = user.firstName
        ur.lastName = user.lastName
        ur.isEnabled = true
        ur.isEmailVerified = true

        val userId = createKeycloakUser(ur, listOf("ROLE_USER"))
        val userResource = realmResource.users().get(userId)
        user.userId = UUID.fromString(userId)

        try {
            userResource.executeActionsEmail(listOf("UPDATE_PASSWORD"))
        } catch (ex: Exception) { logger.error("Email for password reset couldn't be sent to user ${user.username}") }

        return user
    }

    fun updateUser(user: User) {
        user.userId?.let {
            val userResource = realmResource.users().get(it.toString())
            val userRepresentation = try {
                userResource.toRepresentation()
            } catch (e: NotFoundException) {
                throw UserNotFoundException(it)
            } catch (e: ProcessingException) {
                throw AuthServiceUnavailableException(e.message)
            }

            val emailChanged = user.username != userRepresentation.username
            userRepresentation.username = user.username
            userRepresentation.email = user.username
            userRepresentation.firstName = user.firstName
            userRepresentation.lastName = user.lastName

            if (emailChanged) {
                if (!emailValidator.isValid(user.username)) {
                    throw InvalidUsernameException()
                }
                try {
                    userResource.executeActionsEmail(listOf("VERIFY_EMAIL"))
                } catch (ex: Exception) { logger.error("Email for password verification couldn't be sent to user ${user.username}") }
            }
            userResource.update(userRepresentation)
        }
    }

    fun deleteUser(userId: UUID) {
        try {
            realmResource.users().get(userId.toString()).remove()
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }
    }

    fun getUserGroups(userId: UUID) =
        try {
            realmResource.users().get(userId.toString()).groups()
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }

    fun checkUserIsGroupAdmin(groupId: UUID, userId: UUID) {
        val adminGroupId = getGroupAdminId(groupId)
        getUserGroups(userId)
            .any { it.id == adminGroupId } || throw UserNotOrgAdminException(groupId, userId)
    }

    fun getUserOrgs(userId: UUID): List<Organization> =
        getUserGroups(userId).map {
            "/organizations/([^/]+)".toRegex().find(it.path)?.groupValues?.get(0)
        }.filterNotNull()
            .distinct()
            .map { realmResource.getGroupByPath(it).toOrg() }

    fun getKeyOrg(keyId: UUID): Organization =
        try { getUserGroups(keyId) } catch (e: UserNotFoundException) { throw KeyNotFoundException(keyId) }.first().toOrg()

    fun getGroupAdminId(groupId: UUID) =
        getOrgRepresentation(groupId).subGroups.filter { it.name == "admins" }[0].id

    fun getUserRepresentation(userId: UUID) =
        try {
            realmResource.users().get(userId.toString()).toRepresentation()
        } catch (e: NotFoundException) {
            throw UserNotFoundException(userId)
        } catch (e: ProcessingException) {
            throw AuthServiceUnavailableException(e.message)
        }

    fun getUser(userId: UUID) =
        getUserRepresentation(userId).toUser()

    private fun addSubGroup(parent: GroupRepresentation, child: GroupRepresentation): UUID {
        var response: Response = addGroup(child)
        child.id = CreatedResponseUtil.getCreatedId(response)
        response = realmResource.groups().group(parent.id).subGroup(child)
        response.close()
        return UUID.fromString(child.id)
    }

    fun addGroup(g: GroupRepresentation) = realmResource.groups().add(g)

    fun createRole(name: String, composites: List<String>? = null, clientComposites: Map<String, List<String>>? = null) {
        with(RoleRepresentation()) {
            this.name = name
            realmResource.roles().create(this)
        }
        composites?.let {
            realmResource.roles().get(name).addComposites(
                composites.map { realmResource.roles().get(it).toRepresentation() }
            )
        }

        clientComposites?.let {
            it.forEach {
                val client = realmResource.clients()[realmResource.clients().findByClientId(it.key).first().id]
                val createdRole = realmResource.roles().get(name).addComposites(
                    it.value.map {
                        client.roles().get(it).toRepresentation()
                    }
                )
            }
        }
    }

    fun deleteRealm(realmName: String) {
        keycloak.realms().realm(realmName).remove()
    }

    fun updateUserPasswordHash(username: String, hash: String, saltvalue: String, algorithm: String, iterations: Int) {
        try {
            realmResource.users().search(username).filter { it.username == username }.first().id.also { uuid ->
                realmResource.users().get(uuid).toRepresentation().apply {
                    credentials = listOf(
                        CredentialRepresentation().apply {
                            type = CredentialRepresentation.PASSWORD
                            credentialData = mapper.writeValueAsString(mapOf("hashIterations" to iterations, "algorithm" to algorithm))
                            secretData = mapper.writeValueAsString(mapOf("salt" to saltvalue, "value" to hash))
                            isTemporary = false
                        }
                    )
                }.also {
                    realmResource.users().get(uuid).update(it)
                }
            }
        } catch (e: NoSuchElementException) {
            throw UsernameNotFoundException(username)
        }
    }

    fun setRsaKeys(rsa: ComponentRepresentation, rsaEnc: ComponentRepresentation) {
        realmResource.toRepresentation().id.let {
            rsa.parentId = it
            rsaEnc.parentId = it
        }

        realmResource
            .components()
            .query()
            .filter { it.providerType.contains("KeyProvider") && it.name.contains("rsa") }
            .map {
                it.config.add("enabled", "false")
                it.config.add("active", "false")
                realmResource.components().component(it.id).update(it)
            }

        realmResource.components().add(rsa)
        realmResource.components().add(rsaEnc)
    }
}
