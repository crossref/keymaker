package org.crossref.keymaker.service

interface SyncAdminService {
    fun syncPassword(username: String, hash: String, salt: String, algorithm: String, iterations: Int)
}
