package org.crossref.keymaker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KeymakerApplication

fun main(args: Array<String>) {
    runApplication<KeymakerApplication>(*args)
}
