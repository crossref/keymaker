package org.crossref.keymaker.config

import org.crossref.keymaker.api.Subscription
import org.keycloak.representations.idm.ClientRepresentation
import org.keycloak.representations.idm.ProtocolMapperRepresentation

fun getClientDataEnvVar(client: String, dataVar: String): String {
    return "KEYCLOAK_${client.replace("-","").uppercase()}_$dataVar"
}

val realmClients: Map<String, ClientRepresentation> =
    listOf(
        ClientRepresentation().apply {
            clientId = "mycrossref"
            description = "MyCrossref portal client"
            isEnabled = true
            isPublicClient = true
            protocol = "openid-connect"
            isStandardFlowEnabled = true
            isImplicitFlowEnabled = false
            isDirectAccessGrantsEnabled = true
            defaultClientScopes = listOf("roles")
            secret = System.getenv(getClientDataEnvVar(clientId, "SECRET"))
            rootUrl = System.getenv(getClientDataEnvVar(clientId, "ROOTURL"))
            baseUrl = "/"
            protocolMappers = listOf(
                ProtocolMapperRepresentation().apply {
                    name = "email"
                    protocol = "openid-connect"
                    protocolMapper = "oidc-usermodel-property-mapper"
                    config = mapOf(
                        "userinfo.token.claim" to "true",
                        "user.attribute" to "email",
                        "id.token.claim" to "true",
                        "access.token.claim" to "true",
                        "claim.name" to "email",
                        "jsonType.label" to "String"
                    )
                }
            )
            redirectUris = with(getClientDataEnvVar(clientId, "REDIRECTURIS")) {
                (System.getenv(this) ?: throw Exception("Environment variable $this needs to be specified."))
                    .replace(" ", "")
                    .split(",")
            }

            attributes = mutableMapOf(
                "backchannel.logout.session.required" to "true",
                "backchannel.logout.url" to System.getenv(getClientDataEnvVar(clientId, "LOGOUTURL"))
            )
        },

        ClientRepresentation().apply {
            clientId = "keymaker"
            authorizationServicesEnabled = true
            isServiceAccountsEnabled = true
            description = "API for services"
            isEnabled = true
            isBearerOnly = false
            clientAuthenticatorType = "client-secret"
            isDirectAccessGrantsEnabled = false
            isFullScopeAllowed = true
            isImplicitFlowEnabled = false
            protocol = "openid-connect"
            isStandardFlowEnabled = false
            isImplicitFlowEnabled = false
            isDirectAccessGrantsEnabled = true
            secret = System.getenv(getClientDataEnvVar(clientId, "SECRET"))
            rootUrl = System.getenv(getClientDataEnvVar(clientId, "ROOTURL"))
            baseUrl = "/"
            isPublicClient = false
            attributes = mutableMapOf(
                "client_credentials.use_refresh_token" to "true",
                "use.refresh.tokens" to "true"
            )
        },

        ClientRepresentation().apply {
            clientId = "rest-api"
            authorizationServicesEnabled = true
            isServiceAccountsEnabled = true
            description = "API for services"
            isEnabled = true
            isBearerOnly = false
            clientAuthenticatorType = "client-secret"
            isDirectAccessGrantsEnabled = false
            isFullScopeAllowed = true
            isImplicitFlowEnabled = false
            protocol = "openid-connect"
            isStandardFlowEnabled = false
            isImplicitFlowEnabled = false
            isDirectAccessGrantsEnabled = true
            secret = System.getenv(getClientDataEnvVar(clientId, "SECRET"))
            rootUrl = System.getenv(getClientDataEnvVar(clientId, "ROOTURL"))
            baseUrl = "/"
            isPublicClient = false
            attributes = mutableMapOf(
                "client_credentials.use_refresh_token" to "true",
                "use.refresh.tokens" to "true"
            )
        },

        ClientRepresentation().apply {
            clientId = "content-system"
            authorizationServicesEnabled = true
            isServiceAccountsEnabled = true
            description = "API for services"
            isEnabled = true
            isBearerOnly = false
            clientAuthenticatorType = "client-secret"
            isDirectAccessGrantsEnabled = false
            isFullScopeAllowed = true
            isImplicitFlowEnabled = false
            protocol = "openid-connect"
            isStandardFlowEnabled = false
            isImplicitFlowEnabled = false
            isDirectAccessGrantsEnabled = true
            secret = System.getenv(getClientDataEnvVar(clientId, "SECRET"))
            rootUrl = System.getenv(getClientDataEnvVar(clientId, "ROOTURL"))
            baseUrl = "/"
            isPublicClient = false
            attributes = mutableMapOf(
                "client_credentials.use_refresh_token" to "true",
                "use.refresh.tokens" to "true"
            )
        }
    ).associateBy { it.clientId }

val readOnlyResources = mapOf(
    "rest-api" to listOf(Pair("plus-data", "/snapshots/monthly/*")),
    "content-system" to listOf(Pair("plus-data", "/oai*"))
)

val subscriptionToResource = mapOf(
    Subscription.PLUS to mapOf("rest-api" to setOf("plus-data"), "content-system" to setOf("plus-data")),
)

const val realmPasswordPolicy = "passwordHistory(3) and notUsername(undefined) and upperCase(1) and lowerCase(1) and digits(1) and length(8) and specialChars(1) and hashIterations(150000) and hashAlgorithm(pbkdf2-sha512)"
