package org.crossref.keymaker.config

import org.crossref.keymaker.actuators.HealthCheck
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("org.crossref")
class ServicesConfig {

    @Bean
    fun keymakerHealthIndicator(): HealthIndicator {
        return HealthCheck()
    }
}
