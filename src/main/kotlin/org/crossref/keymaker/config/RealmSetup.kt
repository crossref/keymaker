package org.crossref.keymaker.config

import org.crossref.keymaker.service.impl.keycloak.KeycloakConnector
import org.keycloak.representations.idm.GroupRepresentation
import org.keycloak.representations.idm.UserRepresentation
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

class RealmSetup(val realmName: String, val keycloakUrl: String, val rootPwd: String, val kcAdminUserPwd: String, val syncPwd: String) {
    val logger = LoggerFactory.getLogger(this::class.java)!!

    val keycloakSuperUser = "admin"
    val keycloakAdminUser = "crossref"

    var kcConn: KeycloakConnector = KeycloakConnector(keycloakUrl, "master", keycloakSuperUser, rootPwd)

    fun setUpRealm(): Boolean {
        if (kcConn.getRealms().toSet().contains(realmName)) {
            return false
        }

        logger.info("[$realmName] realm does NOT exist, creating realm...")

        // Making sure secrets are defined
        realmClients.values.forEach {
            it.secret ?: throw Exception("No secret setup for '${it.clientId}' client, specify using KEYCLOAK_${it.clientId.replace("-","").uppercase()}_SECRET environment var.")
            it.rootUrl ?: throw Exception("No root URL setup for '${it.clientId}' client, specify using KEYCLOAK_${it.clientId.replace("-","").uppercase()}_ROOTURL environment var.")
        }

        kcConn.createRealm(realmName)
        kcConn.setRealm(realmName)

        kcConn.createRole("ROLE_USER")
        kcConn.createRole("ROLE_APIKEY")
        kcConn.createRole("ROLE_PWDSYNC")
        kcConn.createRole("ROLE_STAFF", listOf("ROLE_USER", "ROLE_APIKEY"))
        kcConn.createRole(
            "ROLE_ADMIN",
            listOf("ROLE_STAFF", "ROLE_USER", "ROLE_APIKEY"),
            mapOf(
                "realm-management" to listOf(
                    "manage-users",
                    "view-realm",
                    "manage-clients",
                    "manage-authorization"
                )
            )
        )

        kcConn.createKeycloakUser(
            UserRepresentation().apply {
                username = keycloakAdminUser
                isEmailVerified = true
                isEnabled = true
            },
            listOf("ROLE_ADMIN"), kcAdminUserPwd
        )

        kcConn.createKeycloakUser(
            UserRepresentation().apply {
                username = "sync"
                isEmailVerified = true
                isEnabled = true
            },
            listOf("ROLE_PWDSYNC", "ROLE_APIKEY"), syncPwd
        )

        kcConn.addGroup(
            GroupRepresentation().apply {
                name = "organizations"
                path = "/organizations"
            }
        )

        realmClients.values.forEach {
            kcConn.createClient(it)
        }

        readOnlyResources.forEach { (clientId, resources) -> resources.forEach { (name, path) -> kcConn.addReadOnlyGroupResource(clientId, name, path) } }

        kcConn.setRealmEventsLogging(true)
        kcConn.setRealmPasswordPolicy(realmPasswordPolicy)

        logger.info("[$realmName] realm created!")
        return true
    }

    fun deleteRealm() {
        kcConn.deleteRealm(realmName)
    }
}

@Component
class InitSetup {
    @Value("\${config.keycloak.admin.url}")
    private lateinit var url: String

    @Value("\${config.keycloak.admin.realm}")
    private lateinit var realm: String

    @Value("\${config.keycloak.admin.rootpwd}")
    private lateinit var rootPwd: String

    @Value("\${config.keycloak.admin.kcadminpwd}")
    private lateinit var kcAdminPwd: String

    @Value("\${config.keycloak.admin.syncpwd}")
    private lateinit var syncPwd: String

    @EventListener(ApplicationReadyEvent::class)
    fun runAfterStartup() {
        RealmSetup(realm, url, rootPwd, kcAdminPwd, syncPwd).also { it.setUpRealm() }
    }
}
