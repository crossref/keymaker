import os
import json
import code
import uuid
from dill.source import getsource

KEYMAKER_URL="http://localhost:9191"
KEYCLOAK_URL=f"http://{os.environ.get('KEYCLOAK_SERVER','docker.local')}:8888"

try:
    from keycloak import KeycloakAdmin, KeycloakOpenID
    import readline
    from pprint import pprint as pp
    import requests
except:
    print ("""make sure you have installed the following
pip install python-keycloak
pip install requests
""")
    exit(1)
try:
    readline.read_history_file(os.path.join(os.environ['HOME'],'.keymakertester'))
except:
    pass

tok=""

def request(func):
    def wrapper(*args,**kwargs):
        global tok
        if not tok:
            print ("you have not logged in , ples use login()")
            return
        prn = kwargs.get("prn",True)
        if prn: 
            print(
                "=============== " + KEYMAKER_URL + args[0] + " ====================="
            )
        resp = func(*args,**kwargs)

        content = resp.content.decode()
        if 'application/json' in resp.headers.get('Content-Type',""):
            content = json.loads(content)
        if prn:
            pp(content)
            print(f"=================== {resp.status_code} ========================")
        return content
    wrapper.__outer__ = func.__name__
    return wrapper

@request
def do_GET(path,**kwargs):
   global tok
   headers = {"Authorization": "Bearer " + tok}
   return requests.get(KEYMAKER_URL + path, headers=headers)

@request
def do_POST(path,contents,**kwargs):
   global tok
   headers = {"Authorization": "Bearer " + tok}
   if type(contents)==str:
       headers['Content-type']= "Application/x-www-form-urlencoded"
       return requests.post(KEYMAKER_URL + path, headers=headers, data=contents)
   elif type (contents==dict):
       return requests.post(KEYMAKER_URL + path, headers=headers, json=contents)

@request
def do_PUT(path,contents,**kwargs):
   global tok
   headers = {"Authorization": "Bearer " + tok}
   return requests.put(KEYMAKER_URL + path, headers=headers, json=contents)

@request
def do_DELETE(path,**kwargs):
   global tok
   headers = {"Authorization": "Bearer " + tok}
   return requests.delete(KEYMAKER_URL + path, headers=headers)

def login(user="crossref",pwd="pwdPWD1!"):

    print (f" ##### Logging in as {user} #####")

    global tok
    k = KeycloakOpenID(
       server_url=f"{KEYCLOAK_URL}/auth/",
       client_id="mycrossref",
       realm_name="crossref",
    )
    token = k.token(user, pwd)
    tok = token["access_token"]
    r = token["refresh_token"]

    print (f" ================== > Logged in as {user}!")

def check(f,*pars,tests = None):
    callstr = f"{f.__outer__}({','.join(repr(i) for i in pars)})"

    try: 
        newres = f(*pars,prn=False)
    except:
        print(f"ERROR: {callstr} failed with a exception")
        return
        
    errors=[]
    if tests:
        for i in tests:
            try:
                ret = i(newres)
            except:
                ret = False
            if not ret:
                errors.append(getsource(i))

    if not errors:
        print(f"PASS: {callstr}")
        return ret
    else:
        print(f"FAIL: {callstr}:\n{''.join(errors)}")
        pp(newres)
        print("=============================================")

    return None


def tests():
    user = str(uuid.uuid4())[:8]+"@domain.com"
    org = "test_org_"+str(uuid.uuid4())[:8]

    login()

    check(do_GET, "/keymaker/api/v1/organization", tests= [
        lambda x: len(x)>0,
        lambda x: all(('orgId' in i and 'sugarId' in i) for i in x)
    ])

    check(do_POST, "/keymaker/api/v1/organization",{"id":"12341234","name":org,"sugarId":1234,"contact":"nobody"}, tests= [
        lambda x: x['name']==org
    ])

    orgid = check(do_GET, f"/keymaker/api/v1/organization?name={org}", tests= [
        lambda x: x[0]['name'] == org,
        lambda x: x[0]['contact'] == 'nobody',
        lambda x: x[0]['sugarId'] == 1234,
        lambda x: x[0]['description'] == '',
        lambda x: x[0]['orgId']
    ])

    check(do_PUT, f"/keymaker/api/v1/organization/{orgid}", {"orgId": orgid, "name":org+"_2","sugarId":1,"contact":"me","description":"desc"}, tests= [
        lambda x: x==''
    ])

    check(do_GET, f"/keymaker/api/v1/organization?name={org}_2", tests= [
        lambda x: x[0]['name'] == org+"_2",
        lambda x: x[0]['contact'] == 'me',
        lambda x: x[0]['sugarId'] == 1,
        lambda x: x[0]['description'] == 'desc',
    ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}", tests= [
        lambda x: x['name'] == org+"_2",
        lambda x: x['contact'] == 'me',
        lambda x: x['sugarId'] == 1,
        lambda x: x['description'] == 'desc',
    ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}/users", tests= [ lambda x: len(x) == 0, ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}/admins", tests= [ lambda x: len(x) == 0, ])

    uid = check(do_POST, f"/keymaker/api/v1/user",{"username":user,"firstName":"fname"}, tests= [
        lambda x: x["username"] == user,
        lambda x: x["firstName"] == "fname",
        lambda x: x["lastName"] == None,
        lambda x: x["userId"]
    ])
    
    check(do_PUT, f"/keymaker/api/v1/user/{uid}",{"userId":uid, "username":user,"firstName":"1","lastName":"2"}, tests= [
        lambda x: x==''
    ])

    check(do_POST, f"/keymaker/api/v1/organization/{orgid}/admins/{uid}",None,tests= [ lambda x: x == '', ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}/admins", tests= [
        lambda x: len(x) == 1,
        lambda x: x[0]['firstName'] == '1',
    ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}/users", tests= [
        lambda x: len(x) == 1,
        lambda x: x[0]['firstName'] == '1',
    ])

    check(do_DELETE, f"/keymaker/api/v1/organization/{orgid}/admins/{uid}",tests= [ lambda x: x == '', ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}/users", tests= [ lambda x: len(x) == 0, ])

    check(do_GET, f"/keymaker/api/v1/organization/{orgid}/admins", tests= [ lambda x: len(x) == 0, ])

    check(do_GET, f"/keymaker/api/v1/user/{uid}", tests= [
        lambda x: x["firstName"] == "1",
        lambda x: x["lastName"] == "2",
    ])

    check(do_GET, f"/keymaker/api/v1/user?firstName=1&lastName=2", tests= [
        lambda x: x[0]['userId'] == uid
    ])


    check(do_DELETE, f"/keymaker/api/v1/user/{uid}", tests= [ lambda x: x=='' ])

    check(do_GET, f"/keymaker/api/v1/user?firstName=1&lastName=2", tests= [
        lambda x: len(x)==0
    ])

    check(do_PUT, f"/keymaker/api/v1/organization/{orgid}/subscription/PLUS/enable",None,tests= [ lambda x: x=='' ])
    check(do_PUT, f"/keymaker/api/v1/organization/{orgid}/subscription/PLUS/enable",None,tests= [ 
        lambda x: "already subscribed to PLUS" in x['message']
    ])

    check(do_GET, "/keymaker/api/v1/organization?subscription=PLUS", tests= [
        lambda x: any(i['name']==org+"_2" for i in x)
    ])

    check(do_PUT, f"/keymaker/api/v1/organization/{orgid}/subscription/PLUS/disable",None,tests= [ lambda x: x=='' ])
    check(do_PUT, f"/keymaker/api/v1/organization/{orgid}/subscription/PLUS/disable",None,tests= [ 
        lambda x: "already unsubscribed" in x['message']
    ])

    check(do_GET, "/keymaker/api/v1/organization?subscription=PLUS", tests= [
        lambda x: all(i['name']!=org+"_2" for i in x)
    ])

    check(do_DELETE, f"/keymaker/api/v1/organization/{orgid}", tests= [ lambda x: x=='' ])

    check(do_GET, f"/keymaker/api/v1/organization?name={org}", tests= [
        lambda x: len(x)==0
    ]

    )

    login("test_admin")

    org1id=check(do_GET, "/keymaker/api/v1/organization", tests= [
        lambda x: len(x)==1,
        lambda x: x[0]['name']=="org1",
        lambda x: x[0]['orgId']
    ])

    check(do_GET,f"/keymaker/api/v1/organization/{org1id}/keys", tests= [
        lambda x: len(x)==0,
    ])

    key = check(do_POST,f"/keymaker/api/v1/key/{org1id}", {"keyId": "2ECAAF3C-C6EB-4D9F-905A-EF65EB63EB5E", "description":"lol", "issuedBy":"2ECAAF3C-C6EB-4D9F-905A-EF65EB63EB5E", "enabled": True} ,tests= [
        lambda x: x['key']['description']=='lol',
        lambda x: x['key']
    ])

    keyid=key['keyId']
    key['description']='lol2'

    login()

    check(do_PUT,f"/keymaker/api/v1/key/{keyid}",key, tests= [
        lambda x: x==""
    ])

    check(do_GET,f"/keymaker/api/v1/organization/{org1id}/keys", tests= [
        lambda x: any(i['description']=='lol2' for i in x)
    ])

    check(do_PUT,f"/keymaker/api/v1/key/{keyid}/disable",{}, tests= [ lambda x: x == '' ])
    check(do_PUT,f"/keymaker/api/v1/key/{keyid}/disable",{}, tests= [ lambda x: x['status'] == "CONFLICT" ])

    login("test_admin")

    check(do_DELETE,f"/keymaker/api/v1/key/{keyid}", tests= [ lambda x: x == '', ])

    check(do_GET,"/keymaker/api/v1/organization/c5e92d00-39c8-418a-9d6f-c490ea1c45a8/keys", tests=[
        lambda x : x['status'] == 'NOT_FOUND'
    ])

    check(do_DELETE,'/keymaker/api/v1/key/9aa35f81-93dd-44c5-867e-39162faf481f', tests=[
        lambda x: x['message'] == "Key with ID 9aa35f81-93dd-44c5-867e-39162faf481f not found"
    ])

msg="""Keymaker tester
Use:
  - login(user,pwd)
  - do_GET("/api/v1/example")
  - do_POST(url,{"object","contents"}) 
  - do_PUT(url,{"object","contents"}) 
  - do_DELETE(url)

  - tests()     # Will run a big set of premade tests
\n\n
Use CTRL+D or exit() to quit\n\n"""

login()

def doq(path,t):
   headers = {"Authorization": "Bearer " + t}
   return requests.get(KEYCLOAK_URL + path, headers=headers)




code.interact(local=locals(), banner=msg)
readline.write_history_file(os.path.join(os.environ['HOME'],'.keymakertester'))

